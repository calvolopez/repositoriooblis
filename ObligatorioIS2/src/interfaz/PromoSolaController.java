package interfaz;

import dominio.Envase;
import dominio.Promocion;
import dominio.Sistema;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import tads.Asociacion;

public class PromoSolaController implements Initializable {

    @FXML
    private ImageView imagenFondo;
    @FXML
    private Text txtNombreProducto;
    @FXML
    private Text txtDatoDescripcion;
    @FXML
    private Text txtDatoPrecio;
    @FXML
    private ImageView imgImagenProd;
    @FXML
    private Text txtCodigoProducto;
    @FXML
    private VBox vbCajaEnvases;
    @FXML
    private Button btnAgregarPromo;
    @FXML
    private Button botonEstadisticas;
    @FXML
    private Button botonProductos;
    @FXML
    private Button botonMapa;
    @FXML
    private Button botonPromo;
    @FXML
    private Button botonVerCarrito;
    @FXML
    private Text txtPrecio;
    @FXML
    private Text txtCantidad;
    @FXML
    private Button btnMenosCant;
    @FXML
    private Button btnMasCant;
    @FXML
    private Text txtLabelCantidad;
    @FXML
    private VBox cajaError;
    @FXML
    private Text txtTituloError;
    @FXML
    private Text txtDescrError;
    @FXML
    private Text txtCarrito;

    private ToggleGroup grupoRB = new ToggleGroup();
    private Sistema sistema;
    private Promocion promo;
    private ManejadorVentanas manejador;

    /**
     * Initializes the controller class.
     */
    public void initComponentes() {
        txtCodigoProducto.setText(promo.getProducto().getCodigo() + "");
        txtDatoDescripcion.setText(promo.getDescripcion());
        txtNombreProducto.setText(promo.getProducto().getNombre());
        imagenFondo.setImage(new Image("/interfaz/Imagenes/plantillaCliente.png"));
        imgImagenProd.setImage(new Image(promo.getProducto().getUrlImagen()));
        txtDatoPrecio.setText("$" + promo.getPrecio());
        txtCantidad.setText("1");

        if (promo.getEsDescuento()) {
            txtPrecio.setText("Precio x1 (descuento)");
        } else {
            txtPrecio.setText("Precio promo " + promo.getValor() + "x" + promo.getValor2());
        }

        ponerEnvases();
        this.txtCarrito.setText(sistema.getCarrito().getProductos().size() + sistema.getCarrito().getPromos().size() + "");
    }

    public void ponerEnvases() {
        //Ponemos los posibles envases en la gridTabla
        ArrayList<Envase> envases = promo.getProducto().getEnvasesSugeridos();

        GridPane gridEnvases = new GridPane();
        gridEnvases.setPrefWidth(335);
        gridEnvases.setMaxSize(335, 300);

        vbCajaEnvases.getChildren().add(gridEnvases);

        //Setteamos el ancho de cada columna y fila
        gridEnvases.getColumnConstraints().add(new ColumnConstraints(111)); // column 0 is 205 wide
        gridEnvases.getColumnConstraints().add(new ColumnConstraints(111));
        gridEnvases.getColumnConstraints().add(new ColumnConstraints(111));
        gridEnvases.getRowConstraints().add(new RowConstraints());
        gridEnvases.getRowConstraints().add(new RowConstraints());
        gridEnvases.getRowConstraints().add(new RowConstraints());

        gridEnvases.getColumnConstraints().get(0).setHalignment(HPos.CENTER);
        gridEnvases.getColumnConstraints().get(1).setHalignment(HPos.CENTER);
        gridEnvases.getColumnConstraints().get(2).setHalignment(HPos.CENTER);
        gridEnvases.getRowConstraints().get(0).setValignment(VPos.CENTER);
        gridEnvases.getRowConstraints().get(1).setValignment(VPos.CENTER);
        gridEnvases.getRowConstraints().get(2).setValignment(VPos.CENTER);

        gridEnvases.setVgap(10);

        for (int i = 0; i < envases.size(); i++) {
            Text nombre = new Text(envases.get(i).getNombre());
            RadioButton rb = new RadioButton("");
            rb.setId(envases.get(i).getCodigo() + "");  //Lo conocemos por su codigo
            rb.setToggleGroup(grupoRB);
            ImageView icono = new ImageView(envases.get(i).getUrlImg());
            icono.setPreserveRatio(true);
            icono.setFitHeight(50);

            nombre.setWrappingWidth(105);
            nombre.setTextAlignment(TextAlignment.CENTER);

            gridEnvases.add(nombre, i, 0);
            gridEnvases.add(icono, i, 1);
            gridEnvases.add(rb, i, 2);
        }
    }

    //Busca el envase con el codigo del envase seleccionado, y lo retorna
    public Envase retornarEnvaseSeleccionado() {
        boolean seSeleccionoEnvase = false;
        for (int i = 0; i < grupoRB.getToggles().size(); i++) {
            if (grupoRB.getToggles().get(i).isSelected()) {
                seSeleccionoEnvase = true;
            }
        }

        if (seSeleccionoEnvase) {
            RadioButton r = (RadioButton) grupoRB.getSelectedToggle();
            int codigoEnvase = Integer.parseInt(r.getId());

            ArrayList<Envase> listaEnvases = sistema.getEnvases();
            for (int i = 0; i < listaEnvases.size(); i++) {
                if (listaEnvases.get(i).getCodigo() == codigoEnvase) {
                    return listaEnvases.get(i);
                }
            }
        }

        return null;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void btnAgregarAccion(ActionEvent event) throws IOException {
        Envase e = retornarEnvaseSeleccionado();

        if (sistema.esUnNumero(txtCantidad.getText()) && e != null) {
            int cant = Integer.parseInt(txtCantidad.getText());
            Asociacion<Promocion, Integer> a = new Asociacion(this.promo, cant);

            if (!sistema.getCarrito().getPromos().contains(a)) {
                this.sistema.getCarrito().getPromos().add(a);
                this.sistema.getCarrito().setCosto(sistema.getCarrito().getCosto() + cant * promo.getPrecio());
            } else {
                int indice = sistema.getCarrito().getPromos().indexOf(a);
                int cantActual = sistema.getCarrito().getPromos().get(indice).getRango();
                if (cantActual + cant > 9) //Para que la cantidad NO sea mayor a 30
                {
                    sistema.getCarrito().getPromos().get(indice).setRango(10);
                } else {
                    sistema.getCarrito().getPromos().get(indice).setRango(cant + cantActual);
                }

                sistema.getCarrito().setCosto(sistema.getCarrito().getCosto() + promo.getPrecio() * cant);
                this.manejador.ventanaDeAviso("AVISO", "Se modifico el carrito.", "Se modifico la cantidad de la promo de '" + this.promo.getProducto().getNombre() + "' en el carrito.");
            }
            this.manejador.cambiarVentanaPromos();
        } else {
            this.manejador.mostrarError(cajaError, txtTituloError, txtDescrError, "ERROR", "Debe seleccionar un envase para agregar la promo al carrito.");
        }
    }

    @FXML
    private void botonEstadisticasClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaEstadistica();
    }

    @FXML
    private void botonProductosClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaProductos();
    }

    @FXML
    private void botonMapaClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaMapa();
    }

    @FXML
    private void botonPromoClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaPromos();
    }

    @FXML
    private void botonVerCarritoClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaCarrito();
    }

    public void setSistema(Sistema s) {
        this.sistema = s;
    }

    public void setPromo(Promocion p) {
        this.promo = p;
    }

    public void setManejador(ManejadorVentanas m) {
        this.manejador = m;
    }

    @FXML
    private void btnMenosAccion(ActionEvent event) {
        int cant = Integer.parseInt(txtCantidad.getText());

        if (cant >= 9) {
            btnMasCant.setDisable(false);
            txtCantidad.setText(cant - 1 + "");

        } else if (cant > 1) {
            txtCantidad.setText(cant - 1 + "");
        } else {
            btnMenosCant.setDisable(true);
        }
    }

    @FXML
    private void btnMasAccion(ActionEvent event) {
        int cant = Integer.parseInt(txtCantidad.getText());

        if (cant == 1) {
            btnMenosCant.setDisable(false);
        } else if (cant >= 9) {
            btnMasCant.setDisable(true);
        }
        txtCantidad.setText(cant + 1 + "");

    }

}
