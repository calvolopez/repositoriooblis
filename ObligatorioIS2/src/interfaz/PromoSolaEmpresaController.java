package interfaz;

import dominio.Envase;
import dominio.Promocion;
import dominio.Sistema;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class PromoSolaEmpresaController implements Initializable {

    @FXML
    private ImageView imagenFondo;
    @FXML
    private Text txtNombreProducto;
    @FXML
    private Text txtDatoDescripcion;
    @FXML
    private Text txtPrecio;
    @FXML
    private Text txtDatoPrecio;
    @FXML
    private ImageView imgImagenProd;
    @FXML
    private Text txtCodigoProducto;
    @FXML
    private VBox vbCajaEnvases;
    @FXML
    private Button botonEstadisticas;
    @FXML
    private Button botonProductos;
    @FXML
    private Button botonMapa;
    @FXML
    private Button botonPromo;
    @FXML
    private Button btnEliminarPromo;

    private Sistema sistema;
    private Promocion promo;
    private ManejadorVentanas manejador;

    /**
     * Initializes the controller class.
     */
    public void initComponentes() {
        txtCodigoProducto.setText(promo.getProducto().getCodigo() + "");
        txtDatoDescripcion.setText(promo.getDescripcion());
        txtNombreProducto.setText(promo.getProducto().getNombre());
        imagenFondo.setImage(new Image("/interfaz/Imagenes/plantillaEmpresa.png"));
        imgImagenProd.setImage(new Image(promo.getProducto().getUrlImagen()));
        txtDatoPrecio.setText("$" + promo.getPrecio());

        if (promo.getEsDescuento()) {
            txtPrecio.setText("Precio x1 (descuento)");
        } else {
            txtPrecio.setText("Precio promo " + promo.getValor() + "x" + promo.getValor2());
        }

        escribirEnvases();
    }

    private void escribirEnvases() {
        GridPane tablaExterna = new GridPane();
        tablaExterna.setPrefWidth(334);
        tablaExterna.setMaxWidth(334);
        tablaExterna.setMinWidth(334);

        vbCajaEnvases.getChildren().add(tablaExterna);

        ArrayList<Envase> lista = this.promo.getProducto().getEnvasesSugeridos();
        for (int i = 0; i < lista.size(); i++) {
            Envase env = lista.get(i);
            String nombre = env.getNombre();

            GridPane tabla = new GridPane();            //Cada Casilla tiene una tabla (Prod / Cant / PrecioX1)
            tabla.setMaxWidth(334);
            tabla.setPrefSize(334, 30);
            tabla.setMaxSize(334, 40);

            //Creamos los Componentes a Agregar
            Text titulo = new Text((1 + i) + " - " + nombre);
            titulo.setTextAlignment(TextAlignment.CENTER);

            tabla.getColumnConstraints().add(new ColumnConstraints(15));    //Marge
            tabla.getColumnConstraints().add(new ColumnConstraints(300));   //Contenido (column 0 is 300 wide)
            tabla.getColumnConstraints().add(new ColumnConstraints(20));    //Margen

            //Agregamos cada elemento a la columna que merezca
            tabla.add(titulo, 1, 0);

            GridPane.setValignment(titulo, VPos.CENTER);
            GridPane.setHalignment(titulo, HPos.LEFT);

            tablaExterna.add(tabla, 0, i);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void botonEstadisticasClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaEstadistica();
    }

    @FXML
    private void botonProductosClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaProductos();
    }

    @FXML
    private void botonMapaClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaMapa();
    }

    @FXML
    private void botonPromoClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaPromos();
    }

    @FXML
    private void btnEliminarPromo(ActionEvent event) throws IOException {

        if (this.manejador.ventanaDeConfirmacion("AVISO", "Presione OK para confirmar.", "Si desea eliminar la promoción, presione 'OK'.")) {
            this.sistema.eliminarPromo(promo);
            this.manejador.cambiarVentanaPromos();
        }
    }

    public void setSistema(Sistema s) {
        this.sistema = s;
    }

    public void setManejador(ManejadorVentanas m) {
        this.manejador = m;
    }

    public void setPromo(Promocion p) {
        this.promo = p;
    }

}
