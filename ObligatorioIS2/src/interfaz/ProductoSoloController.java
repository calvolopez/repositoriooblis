package interfaz;

import dominio.Envase;
import dominio.Producto;
import dominio.Sistema;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import tads.Asociacion;

public class ProductoSoloController implements Initializable {

    @FXML
    private Text txtNombreProd;
    @FXML
    private Text txtMateriales;
    @FXML
    private Text txtOrigenMateriales;
    @FXML
    private ImageView imgImagenProd;
    @FXML
    private Text txtCodigoProducto;
    @FXML
    private Text txtCantidad;
    @FXML
    private Button btnMasCant;
    @FXML
    private Button btnMenosCant;
    @FXML
    private Button botonEstadisticas;
    @FXML
    private Button botonProductos;
    @FXML
    private Button botonMapa;
    @FXML
    private Button botonPromo;
    @FXML
    private Button botonVerCarrito;
    @FXML
    private ImageView imagenFondo;
    @FXML
    private VBox vbCajaEnvases;
    @FXML
    private Button btnAgregarAlCarrito;
    @FXML
    private Text txtDatoPrecio;
    @FXML
    private Text txtLabelCantidad;
    @FXML
    private VBox cajaError;
    @FXML
    private Text txtTitulo;
    @FXML
    private Text txtDescr;
    @FXML
    private Text txtCarrito;

    private ToggleGroup grupoRB = new ToggleGroup();
    private Sistema sistema;
    private Producto producto;
    private ManejadorVentanas manejador;

    /**
     * Initializes the controller class.
     */
    public void initComponentes() {
        imagenFondo.setImage(new Image("/interfaz/Imagenes/plantillaCliente.png"));
        imgImagenProd.setImage(new Image(producto.getUrlImagen()));
        txtCantidad.setText("1");
        txtCodigoProducto.setText(producto.getCodigo() + "");
        txtOrigenMateriales.setText(producto.getOrigenMateriaPrima());
        txtMateriales.setText(producto.getMaterial());
        txtNombreProd.setText(producto.getNombre());
        txtDatoPrecio.setText("$" + producto.getPrecio());
        if (producto.isEsComida()) {
            txtLabelCantidad.setText("Cantidad(kg)");
        } else {
            txtLabelCantidad.setText("Cantidad(L)");
        }
        this.ponerEnvases();
        //grupoRB.getToggles().get(0).setSelected(true);

        btnMasCant.setShape(new Circle(1.7));
        btnMenosCant.setShape(new Circle(1.7));
        this.txtCarrito.setText(sistema.getCarrito().getProductos().size() + sistema.getCarrito().getPromos().size() + "");
    }

    public void ponerEnvases() {
        //Ponemos los posibles envases en la gridTabla
        ArrayList<Envase> envases = producto.getEnvasesSugeridos();

        GridPane gridEnvases = new GridPane();
        gridEnvases.setPrefWidth(335);
        gridEnvases.setMaxSize(335, 300);

        vbCajaEnvases.getChildren().add(gridEnvases);

        //Setteamos el ancho de cada columna y fila
        gridEnvases.getColumnConstraints().add(new ColumnConstraints(111)); // column 0 is 205 wide
        gridEnvases.getColumnConstraints().add(new ColumnConstraints(111));
        gridEnvases.getColumnConstraints().add(new ColumnConstraints(111));
        gridEnvases.getRowConstraints().add(new RowConstraints());
        gridEnvases.getRowConstraints().add(new RowConstraints());
        gridEnvases.getRowConstraints().add(new RowConstraints());

        gridEnvases.getColumnConstraints().get(0).setHalignment(HPos.CENTER);
        gridEnvases.getColumnConstraints().get(1).setHalignment(HPos.CENTER);
        gridEnvases.getColumnConstraints().get(2).setHalignment(HPos.CENTER);
        gridEnvases.getRowConstraints().get(0).setValignment(VPos.CENTER);
        gridEnvases.getRowConstraints().get(1).setValignment(VPos.CENTER);
        gridEnvases.getRowConstraints().get(2).setValignment(VPos.CENTER);

        gridEnvases.setVgap(10);

        for (int i = 0; i < envases.size(); i++) {
            Text nombre = new Text(envases.get(i).getNombre());
            RadioButton rb = new RadioButton("");
            rb.setId(envases.get(i).getCodigo() + "");  //Lo conocemos por su codigo
            rb.setToggleGroup(grupoRB);
            ImageView icono = new ImageView(envases.get(i).getUrlImg());
            icono.setPreserveRatio(true);
            icono.setFitHeight(50);

            nombre.setWrappingWidth(105);
            nombre.setTextAlignment(TextAlignment.CENTER);

            gridEnvases.add(nombre, i, 0);
            gridEnvases.add(icono, i, 1);
            gridEnvases.add(rb, i, 2);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void btnMasAccion(ActionEvent event) {
        int cant = Integer.parseInt(txtCantidad.getText());

        if (cant == 1) {
            btnMenosCant.setDisable(false);
        } else if (cant >= 29) {
            btnMasCant.setDisable(true);
        }
        txtCantidad.setText(cant + 1 + "");

    }

    @FXML
    private void btnMenosAccion(ActionEvent event) {
        int cant = Integer.parseInt(txtCantidad.getText());

        if (cant >= 29) {
            btnMasCant.setDisable(false);
            txtCantidad.setText(cant - 1 + "");

        } else if (cant > 1) {
            txtCantidad.setText(cant - 1 + "");
        } else {
            btnMenosCant.setDisable(true);
        }
    }

    @FXML
    private void botonEstadisticasClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaEstadistica();
    }

    @FXML
    private void botonProductosClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaProductos();
    }

    @FXML
    private void botonMapaClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaMapa();
    }

    @FXML
    private void botonPromoClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaPromos();
    }

    @FXML
    private void botonVerCarritoClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaCarrito();
    }

    @FXML
    private void btnAgregarAccion(ActionEvent event) throws IOException {
        //Si en el carrito ya esta el producto, solo modifico su cantidad. Sino, lo agrego
        int cant = Integer.parseInt(txtCantidad.getText());
        Envase e = retornarEnvaseSeleccionado();
        Asociacion<Asociacion<Producto, Integer>, Envase> elem = new Asociacion(new Asociacion(producto, cant), e);

        if (e == null) {
            this.manejador.mostrarError(cajaError, txtTitulo, txtDescr, "ERROR", "Debe seleccionar uno de los envases sugeridos para continuar.");
        } else {
            if (!sistema.getCarrito().getProductos().contains(elem)) {
                sistema.getCarrito().agregarProducto(producto, cant, e);
                // this.manejador.ventanaDeAviso("AVISO", "Producto agregado al carrito", "Se agrego el producto '" + this.producto.getNombre() + "' al carrito exitosamente.");
            } else {
                int indice = sistema.getCarrito().getProductos().indexOf(elem);
                int cantActual = sistema.getCarrito().getProductos().get(indice).getDominio().getRango();
                if (cantActual + cant >= 30) //Para que la cantidad NO sea mayor a 30
                {
                    sistema.getCarrito().getProductos().get(indice).getDominio().setRango(30);
                } else {
                    sistema.getCarrito().getProductos().get(indice).getDominio().setRango(cantActual + cant);
                }
                sistema.getCarrito().setCosto(sistema.getCarrito().getCosto() + producto.getPrecio() * cant);
                // this.manejador.ventanaDeAviso("AVISO", "Se modifico el carrito.", "Se modifico la cantidad del producto '" + this.producto.getNombre() + "' en el carrito.");
            }
            this.manejador.cambiarVentanaProductos();
        }
    }

    //Busca el envase con el codigo del envase seleccionado, y lo retorna
    public Envase retornarEnvaseSeleccionado() {
        boolean seSeleccionoEnvase = false;
        for (int i = 0; i < grupoRB.getToggles().size(); i++) {
            if (grupoRB.getToggles().get(i).isSelected()) {
                seSeleccionoEnvase = true;
            }
        }

        if (seSeleccionoEnvase) {
            RadioButton r = (RadioButton) grupoRB.getSelectedToggle();
            int codigoEnvase = Integer.parseInt(r.getId());

            ArrayList<Envase> listaEnvases = sistema.getEnvases();
            for (int i = 0; i < listaEnvases.size(); i++) {
                if (listaEnvases.get(i).getCodigo() == codigoEnvase) {
                    return listaEnvases.get(i);
                }
            }
        }

        return null;
    }

    public void setSistema(Sistema s) {
        this.sistema = s;
    }

    public void setProducto(Producto p) {
        this.producto = p;
    }

    public void setManejador(ManejadorVentanas m) {
        this.manejador = m;
    }
}
