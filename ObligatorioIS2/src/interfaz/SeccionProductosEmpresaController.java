package interfaz;

import dominio.Producto;
import dominio.Sistema;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class SeccionProductosEmpresaController implements Initializable {

    @FXML
    private AnchorPane apfondo;
    @FXML
    private Button botonEstadisticas;
    @FXML
    private Button botonProductos;
    @FXML
    private Button botonMapa;
    @FXML
    private Button botonPromo;
    @FXML
    private ImageView imagenFondo;
    @FXML
    private Button btnAgregar;

    private Sistema sistema;
    private ManejadorVentanas manejador;

    /**
     * Initializes the controller class.
     */

    public void initComponentes() {
        imagenFondo.setImage(new Image("/interfaz/Imagenes/plantillaEmpresa.png"));
        imprimirProductos();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void imprimirProductos() {
        //Creamos una tabla con 2 COL. En cada casilla va un Prod (Imagen y Nombre). Al clickear la imagen, se muestra la ficha del Prod
        GridPane tabla = new GridPane();
        tabla.setPrefSize(334, 450);
        tabla.setMaxSize(334, 450);

        //tabla.getColumnConstraints().add(new ColumnConstraints(7));
        tabla.getColumnConstraints().add(new ColumnConstraints(334));
        //tabla.getColumnConstraints().add(new ColumnConstraints(7));

        apfondo.getChildren().add(tabla);   //Ponemos la tabla en su lugar

        ArrayList<Producto> lista = sistema.getProductos();
        for (int i = 0; i < lista.size(); i++) {

            HBox caja = new HBox();
            VBox datos = new VBox();
            HBox margen = new HBox();
            HBox margen2 = new HBox();
            HBox margen3 = new HBox();
            HBox margenH = new HBox();

            datos.setPrefSize(170, 200);
            datos.setMinSize(170, 200);
            datos.setMaxWidth(200);
            caja.setPrefSize(334, 200);
            caja.setMinSize(334, 200);
            caja.setMaxSize(334, 200);
            margen.setPrefHeight(15);
            margen.setMinHeight(15);
            margen2.setPrefHeight(20);
            margen2.setMinHeight(20);
            margen3.setPrefHeight(20);
            margen3.setMinHeight(20);
            margenH.setPrefWidth(15);
            margenH.setMinWidth(15);
            caja.setAlignment(Pos.CENTER);
            datos.setAlignment(Pos.TOP_LEFT);

            if (i % 2 == 0) {
                caja.setBackground(new Background(new BackgroundFill(Color.rgb(229, 229, 229), CornerRadii.EMPTY, Insets.EMPTY)));
            }

            ImageView imagen = new ImageView(new Image(lista.get(i).getUrlImagen()));
            imagen.setPreserveRatio(true);
            imagen.setLayoutY(0);
            imagen.setFitHeight(140);

            Text nombre = new Text(lista.get(i).getNombre().toUpperCase());
            Text materiales = new Text("Materiales: " + lista.get(i).getMaterial());
            Text precio = new Text("$" + lista.get(i).getPrecio());
            materiales.setWrappingWidth(170);

            nombre.setLayoutY(165);
            nombre.setLayoutX(20);
            nombre.setFont(Font.font("Trebuchet MS", FontWeight.BOLD, FontPosture.REGULAR, 16));   //Tipo de letra
            nombre.setWrappingWidth(140);
            nombre.setTextAlignment(TextAlignment.LEFT);

            imagen.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    boolean seguir = true;
                    String name = nombre.getText();
                    ArrayList<Producto> lista = sistema.getProductos();
                    for (int j = 0; j < lista.size() && seguir; j++) {
                        Producto p = lista.get(j);
                        if (p.getNombre().toUpperCase().compareTo(name) == 0) {
                            try {
                                manejador.cambiarVentanaProductoSolo(p);
                                seguir = false;
                            } catch (Exception e) {
                                manejador.ventanaDeError("ERROR", "Reinicie el programa", "No se reconoce el producto que se desea inspeccionar");
                            }
                        }
                    }
                }
            });

            datos.getChildren().add(margen);
            datos.getChildren().add(nombre);
            datos.getChildren().add(margen2);
            datos.getChildren().add(materiales);
            datos.getChildren().add(margen3);
            datos.getChildren().add(precio);

            caja.getChildren().add(imagen);     //Agregamos la Imagen a su A.P.
            caja.getChildren().add(margenH);
            caja.getChildren().add(datos);      //Agregamos el Texto a su A.P.

            tabla.add(caja, 0, i);              //Agregamos el A.P. en su Casilla de la TABLA
        }
    }

    @FXML
    private void botonEstadisticasClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaEstadistica();
    }

    @FXML
    private void botonProductosAccion(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaProductos();
    }

    @FXML
    private void botonMapaClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaMapa();
    }

    @FXML
    private void botonPromoClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaPromos();

    }

    @FXML
    private void btnAgregarProdClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaAgregarProducto();
    }

    public void setManejador(ManejadorVentanas m) {
        this.manejador = m;
    }

    public void setSistema(Sistema s) {
        this.sistema = s;
    }

}
