package interfaz;

import dominio.*;
import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class ComprarController implements Initializable {

    @FXML
    private Button botonEstadisticas;
    @FXML
    private Button botonProductos;
    @FXML
    private Button botonMapa;
    @FXML
    private Button botonPromo;
    @FXML
    private Button botonVerCarrito;
    @FXML
    private Text txtCosto;
    @FXML
    private ToggleGroup envio;
    @FXML
    private HBox hboxCajaCostoTotal;
    @FXML
    private ScrollPane spScrollPane;
    @FXML
    private AnchorPane apfondo;
    @FXML
    private Button btnAceptar;
    @FXML
    private Button btnVolver;
    @FXML
    private DatePicker dpFechaRetiro;
    @FXML
    private RadioButton rbLocal;
    @FXML
    private RadioButton rbPreventa;
    @FXML
    private Text txtFecha;
    @FXML
    private ImageView imagenFondo;
    @FXML
    private VBox cajaError;
    @FXML
    private Text txtTitulo;
    @FXML
    private Text txtDescr;
    @FXML
    private Text txtCarrito;

    private Carrito carrito;
    private Sistema sistema;
    private boolean preventa;
    private ManejadorVentanas manejador;

    /**
     * Initializes the controller class.
     */
    public void initComponentes() {
        imagenFondo.setImage(new Image("/interfaz/Imagenes/plantillaCliente.png"));
        this.carrito = sistema.getCarrito();
        txtCosto.setText("$" + carrito.getCosto());

        this.preventa = true;
        this.dpFechaRetiro.setDisable(true);
        this.txtCarrito.setText(sistema.getCarrito().getProductos().size() + sistema.getCarrito().getPromos().size() + "");
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void botonEstadisticasClickeado(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaEstadistica();
    }

    @FXML
    private void botonProductosClickeado(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaProductos();
    }

    @FXML
    private void botonMapaClickeado(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaMapa();
    }

    @FXML
    private void botonPromoClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaPromos();
    }

    @FXML
    private void botonVerCarritoClickeado(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaCarrito();
    }

    @FXML
    private void botonCancelarAccion(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaCarrito();
    }

    @FXML
    private void botonAceptarAccion(ActionEvent event) throws IOException {
        validarDatosYAvanzar();
    }

    //Settea el atributo sistema a s
    public void setSistema(Sistema s) {
        this.sistema = s;
    }

    public void setManejador(ManejadorVentanas m) {
        this.manejador = m;
    }

    private void validarDatosYAvanzar() throws IOException {
        Calendar c = Calendar.getInstance();
        Date fechaVenta = c.getTime();

        if (rbPreventa.isSelected()) {
            if (dpFechaRetiro.getValue() == null) //Si NO selecciono una fecha
            {
                this.manejador.mostrarError(cajaError, txtTitulo, txtDescr, "ERROR", "Debe seleccionar una fecha de retiro para la preventa.");
            } else {
                Date fechaRetiro = java.sql.Date.valueOf(dpFechaRetiro.getValue());
                if (fechaRetiro.compareTo(fechaVenta) > 0 && this.manejador.ventanaDeConfirmacion("ATENCIÓN", "Confirme si desea hacer la compra", "Presione 'OK' si desea proceder y hacer la compra.")) {
                    Venta v = new Venta(sistema.getCarrito().getProductos(), sistema.getCarrito().getPromos(), sistema.getCarrito().getCosto(), sistema.getCliente(), fechaVenta, fechaRetiro, preventa);
                    sistema.agregarVenta(v);
                    sistema.getCliente().agregarVenta(v);
                    this.manejador.cambiarVentanaETicket(v);
                } else {
                    this.manejador.mostrarError(cajaError, txtTitulo, txtDescr, "ERROR", "Seleccione una fecha valida para el retiro de la preventa (mayor a la de hoy).");
                }
            }
        } else {
            if (this.manejador.ventanaDeConfirmacion("ATENCIÓN", "Confirme si desea hacer la compra", "Presione 'OK' si desea proceder y hacer la compra.")) {
                Venta v = new Venta(sistema.getCarrito().getProductos(), sistema.getCarrito().getPromos(), sistema.getCarrito().getCosto(), sistema.getCliente(), fechaVenta, null, preventa);
                sistema.agregarVenta(v);
                sistema.getCliente().agregarVenta(v);
                this.manejador.cambiarVentanaETicket(v);
            }
        }
    }

    @FXML
    private void dpFechaRetiroAccion(ActionEvent event) {
    }

    @FXML
    private void rbLocalAccion(ActionEvent event) {
        dpFechaRetiro.setDisable(true);
        txtFecha.setOpacity(0.5);
        this.preventa = false;
    }

    @FXML
    private void rbPreventaAccion(ActionEvent event) {
        dpFechaRetiro.setDisable(false);
        txtFecha.setOpacity(1);
        this.preventa = true;
    }

}
