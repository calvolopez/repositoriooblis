package interfaz;

import dominio.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import tads.Asociacion;

public class ObligatorioIS2 extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Sistema s = new Sistema();
        ManejadorVentanas m = new ManejadorVentanas(stage, s);

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/interfaz/Inicio.fxml"));
        Parent root = loader.load();
        InicioController ventanaInicio = loader.getController();

        Envase e1 = new Envase("Llevar una botella por litro comprado.", "Botella de Vidrio", "/interfaz/Imagenes/botella.png");
        Envase e7 = new Envase("Llevar una botella por litro comprado.", "Botella de Plástico", "/interfaz/Imagenes/botellaPlas.png");
        Envase e2 = new Envase("Capacidad de al menos 500cm cúbicos.", "Tupper Hermetico", "/interfaz/Imagenes/tupper.png");
        Envase e3 = new Envase("De al menos 5 litros.", "Bidón", "/interfaz/Imagenes/bidon.png");
        Envase e4 = new Envase("Bandeja cerrada (no hermética)", "Bandeja", "/interfaz/Imagenes/bandeja.png");
        Envase e5 = new Envase("De tela preferentemente", "Bolsa de Tela", "/interfaz/Imagenes/bolsa.png");
        Envase e6 = new Envase("Bolsa de tipo ziplock, hermética.", "Ziplock", "/interfaz/Imagenes/ziplock.png");
        ArrayList<Envase> listaEnvasesCoca = new ArrayList();
        ArrayList<Envase> listaEnvasesBirra = new ArrayList();
        ArrayList<Envase> listaEnvasesPicada = new ArrayList();
        ArrayList<Envase> listaEnvasesNachos = new ArrayList();
        listaEnvasesCoca.add(e1);
        listaEnvasesCoca.add(e7);
        listaEnvasesCoca.add(e3);
        listaEnvasesBirra.add(e1);
        listaEnvasesBirra.add(e3);
        listaEnvasesPicada.add(e4);
        listaEnvasesPicada.add(e2);
        listaEnvasesPicada.add(e6);
        listaEnvasesNachos.add(e2);
        listaEnvasesNachos.add(e6);

        Producto p1 = new Producto("Cola Light", "Hoja de Coca, Edulcorante, Agua", "Uruguay", 63, false, "/interfaz/Imagenes/cocaLight.png");
        Producto p2 = new Producto("Cola", "Hoja de Coca, Azucar, Agua", "Uruguay", 63, false, "/interfaz/Imagenes/cocaComun.png");
        Producto p3 = new Producto("Maní la Abundancia", "Cacahuate", "Chile", 105, true, "/interfaz/Imagenes/maniFrito.png");
        Producto p4 = new Producto("Nachos Caseros", "Harina de Maiz, Sal", "Uruguay", 89, true, "/interfaz/Imagenes/nachos.png");
        Producto p5 = new Producto("Queso Farmy", "Leche, Quajo, Conservantes", "Argentina", 130, true, "/interfaz/Imagenes/queso.png");
        Producto p6 = new Producto("Salame Sarubbi", "Carne de cerdo, Grasas, Sal", "Uruguay", 219, true, "/interfaz/Imagenes/salame.png");
        p1.setEnvasesSugeridos(listaEnvasesCoca);
        p2.setEnvasesSugeridos(listaEnvasesCoca);
        p3.setEnvasesSugeridos(listaEnvasesPicada);
        p4.setEnvasesSugeridos(listaEnvasesNachos);
        p5.setEnvasesSugeridos(listaEnvasesPicada);
        p6.setEnvasesSugeridos(listaEnvasesPicada);

        ClienteOnline cl = new ClienteOnline("Alejandro", "Adorjan", "alejandro@gmail.com");

        PuntoDeVenta pto1 = new PuntoDeVenta("Cuareim", "Mercedes", "Centro", 1451, 21231234);
        PuntoDeVenta pto2 = new PuntoDeVenta("Bulevar España", "Enrique Muñoz", "Pocitos", 2643, 22342345);
        PuntoDeVenta pto3 = new PuntoDeVenta("Av Arocena", "Carlos Federico Saez", "Carrasco", 1564, 23453456);
        PuntoDeVenta pto4 = new PuntoDeVenta("Av Gral Rivera", "Colombes", "Malvin", 4502, 24564567);

        Promocion pr1 = new Promocion(p3, "2X1 en maní", false, 2, 1);
        Promocion pr2 = new Promocion(p1, "3X2, no te lo pierdas", false, 3, 2);
        Promocion pr3 = new Promocion(p5, "¡Super oferta! 25% de descuento", true, 25, 0);

        ArrayList<Asociacion<Asociacion<Producto, Integer>, Envase>> productos= new ArrayList();
        productos.add(new Asociacion(new Asociacion(p1,4),e7));
        productos.add(new Asociacion(new Asociacion(p4,2),e6));
        ArrayList<Asociacion<Promocion, Integer>> promos= new ArrayList();
        promos.add(new Asociacion(pr3,1));
        Calendar c = Calendar.getInstance();
        Date fecha = c.getTime();
        int costo= p1.getPrecio()*4 + p4.getPrecio()*2+ pr3.getPrecio();
        Venta v = new Venta(productos,promos,costo, cl,fecha ,null,false);
        
        s.agregarVenta(v);
        s.agregarPuntoVenta(pto1);
        s.agregarPuntoVenta(pto2);
        s.agregarPuntoVenta(pto3);
        s.agregarPuntoVenta(pto4);
        s.agregarProducto(p1);
        s.agregarProducto(p2);
        s.agregarProducto(p3);
        s.agregarProducto(p4);
        s.agregarProducto(p5);
        s.agregarProducto(p6);
        s.agregarEnvase(e1);
        s.agregarEnvase(e2);
        s.agregarEnvase(e3);
        s.agregarEnvase(e4);
        s.agregarEnvase(e5);
        s.agregarEnvase(e6);
        s.agregarEnvase(e7);
        s.agregarCliente(cl);
        s.agregarPromo(pr3);
        s.agregarPromo(pr2);
        s.agregarPromo(pr1);
        s.setCliente(cl);

        ventanaInicio.setManejador(m);
        ventanaInicio.initComponentes();

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
