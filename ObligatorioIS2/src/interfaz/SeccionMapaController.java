package interfaz;

import dominio.PuntoDeVenta;
import dominio.Sistema;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class SeccionMapaController implements Initializable {

    @FXML
    private AnchorPane apLienzo;
    @FXML
    private ImageView imgFondo;
    @FXML
    private Button btnCarro;
    @FXML
    private ScrollPane spPanelPPal;
    @FXML
    private Group grpPanel;
    @FXML
    private ImageView imgFondoPanel;
    @FXML
    private GridPane gridEstructuraPanel;
    @FXML
    private Text txtTitulo;
    @FXML
    private Text txtProducto;
    @FXML
    private AnchorPane apSeccion2;
    @FXML
    private Button botonEstadisticas;
    @FXML
    private Button botonProductos;
    @FXML
    private Button botonMapa;
    @FXML
    private Button botonPromo;
    @FXML
    private AnchorPane apDirecciones;
    @FXML
    private WebView webViewMapa;
    @FXML
    private Text txtCarrito;

    private ManejadorVentanas manejador;
    private Sistema sistema;

    /**
     * Initializes the controller class.
     */
    //METODO
    public void initComponentes() {
        if (!this.manejador.getEsCliente()) {
            //El fondo ser la "PlantillaBasica" de 'Empresa' (que tiene algunas diferencias)
            imgFondo.setImage(new Image("/interfaz/Imagenes/plantillaEmpresa.png"));
            btnCarro.setDisable(true);
            this.txtCarrito.setText(sistema.getCarrito().getProductos().size() + sistema.getCarrito().getPromos().size() + "");
        } else {
            imgFondo.setImage(new Image("/interfaz/Imagenes/plantillaCliente.png"));
            btnCarro.setDisable(false);
        }

        this.imprimirLocales();

    }

    private void imprimirLocales() {
        VBox caja = new VBox();
        caja.setMaxWidth(335);
        caja.setMinSize(335, 200);

        apDirecciones.getChildren().add(caja);
        caja.setSpacing(20);

        //Agregaremos Grids con los locales a la VBox
        ArrayList<PuntoDeVenta> lista = sistema.getPuntosDeVenta();
        for (int i = 0; i < lista.size(); i++) {
            PuntoDeVenta pto = lista.get(i);

            GridPane tabla = new GridPane();            //Cada Casilla tiene una tabla (Prod / Cant / PrecioX1)
            tabla.setMaxWidth(335);
            tabla.setMinSize(335, 50);

            if (i % 2 == 0) //Pintaremos su fondo de gris claro
            {
                tabla.setBackground(new Background(new BackgroundFill(Color.grayRgb(1, 0.06), CornerRadii.EMPTY, Insets.EMPTY)));
            }

            //Creamos los Componentes a Agregar
            Text titulo = new Text("Barrio " + pto.getBarrio());
            Text direccion = new Text("Dir: " + pto.getCalle() + " " + pto.getNumeroPuerta());
            Text telefono = new Text("Tel: " + pto.getTelefono());

            //Ajustamos la estetica del texto
            titulo.setFont(Font.font("Trebuchet MS", FontWeight.BOLD, FontPosture.REGULAR, 16));
            direccion.setFont(Font.font("Trebuchet MS", FontWeight.NORMAL, FontPosture.REGULAR, 14));
            telefono.setFont(Font.font("Trebuchet MS", FontWeight.NORMAL, FontPosture.REGULAR, 14));
            titulo.setFill(Paint.valueOf("1fc1cb"));

            titulo.setTextAlignment(TextAlignment.LEFT);
            direccion.setTextAlignment(TextAlignment.LEFT);
            telefono.setTextAlignment(TextAlignment.LEFT);

            tabla.getColumnConstraints().add(new ColumnConstraints(10));    //Esta columna es solo para margen
            tabla.getColumnConstraints().add(new ColumnConstraints(284));   //column 0 is 285 wide
            tabla.getColumnConstraints().add(new ColumnConstraints(40));    //Margen

            //Agregamos cada elemento a la columna que merezca
            tabla.add(titulo, 1, 0);
            tabla.add(direccion, 1, 1);
            tabla.add(telefono, 1, 2);

            caja.getChildren().add(tabla);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        WebEngine engine = webViewMapa.getEngine();
        engine.load("file:"+Paths.get("").toAbsolutePath().toString()+"/src/interfaz/Mapa.html");
    }

    @FXML
    private void botonEstadisticasClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaEstadistica();
    }

    @FXML
    private void botonMapaClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaMapa();
    }

    @FXML
    private void botonProductosAccion(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaProductos();
    }

    @FXML
    private void botonCarroAccion(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaCarrito();
    }

    @FXML
    private void botonPromoClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaPromos();
    }

    public void setSistema(Sistema s) {
        this.sistema = s;
    }

    public void setManejador(ManejadorVentanas m) {
        this.manejador = m;
    }
}
