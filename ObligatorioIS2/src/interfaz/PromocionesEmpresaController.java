package interfaz;

import dominio.Promocion;
import dominio.Sistema;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class PromocionesEmpresaController implements Initializable {

    @FXML
    private AnchorPane apFondo;
    @FXML
    private Button botonMapa;
    @FXML
    private Button botonPromo;
    @FXML
    private Button botonEstadisticas;
    @FXML
    private Button botonProductos;
    @FXML
    private Button btnAgregar;

    private Sistema sistema;
    private ManejadorVentanas manejador;

    /**
     * Initializes the controller class.
     */
    public void initComponents() {
        this.imprimirProductos();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void imprimirProductos() {
        GridPane tabla = new GridPane();
        ArrayList<Promocion> promos = sistema.getPromos();
        for (int i = 0; i < promos.size(); i++) {
            Promocion p = promos.get(i);
            AnchorPane ap = new AnchorPane();

            ImageView imagen = new ImageView(p.getProducto().getUrlImagen());
            imagen.setPreserveRatio(true);
            imagen.setLayoutY(10);
            imagen.setLayoutX(20);
            imagen.setFitHeight(125);

            Text txt = new Text(p.getDescripcion());
            txt.setLayoutY(165);
            txt.setLayoutX(20);
            txt.setFont(Font.font("Trebuchet MS", FontWeight.BOLD, FontPosture.REGULAR, 16));   //Tipo de letra
            txt.setWrappingWidth(140);

            imagen.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    boolean seguir = true;
                    String name = txt.getText();
                    ArrayList<Promocion> lista = sistema.getPromos();
                    for (int j = 0; j < lista.size() && seguir; j++) {
                        Promocion p = lista.get(j);
                        if (p.getDescripcion().compareTo(name) == 0) {
                            try {
                                seguir = false;
                                manejador.cambiarVentanaPromoSola(p);
                            } catch (Exception e) {
                                manejador.ventanaDeError("ERROR", "Reinicie el programa", "No se reconoce la promoción que se desea inspeccionar");
                            }
                        }
                    }
                }
            });

            ap.getChildren().add(imagen);
            ap.getChildren().add(txt);
            tabla.add(ap, i % 2, i / 2);
        }
        apFondo.getChildren().add(tabla);
    }

    @FXML
    private void botonMapaClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaMapa();
    }

    @FXML
    private void botonPromoClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaPromos();
    }

    @FXML
    private void botonEstadisticasClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaEstadistica();
    }

    @FXML
    private void botonProductosAccion(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaProductos();
    }

    @FXML
    private void btnAgrgClck(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaAgregarPromo();
    }

    public void setSistema(Sistema s) {
        this.sistema = s;
    }

    public void setManejador(ManejadorVentanas m) {
        this.manejador = m;
    }
}
