package interfaz;

import dominio.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class AgregarProductoController implements Initializable {

    @FXML
    private TextField tnombre;
    @FXML
    private TextField tmaterial;
    @FXML
    private TextArea torigen;
    @FXML
    private Button btnListo;
    @FXML
    private TextField tprecio;
    @FXML
    private ScrollPane spScrollPane;
    @FXML
    private AnchorPane apfondo;
    @FXML
    private Button btnVolver;
    @FXML
    private ComboBox<Envase> cbEnvase1;
    @FXML
    private ComboBox<Envase> cbEnvase2;
    @FXML
    private ComboBox<Envase> cbEnvase3;
    @FXML
    private ToggleGroup tipoAlimento;
    @FXML
    private ImageView imagenFondo;
    @FXML
    private RadioButton rbBebida;
    @FXML
    private RadioButton rbComida;
    @FXML
    private Button botonEstadisticas;
    @FXML
    private Button botonProductos;
    @FXML
    private Button botonMapa;
    @FXML
    private Button botonPromo;
    @FXML
    private VBox cajaError;
    @FXML
    private Text txtTitulo;
    @FXML
    private Text txtDescr;

    private ManejadorVentanas manejador;
    private Sistema sistema;
    private Envase cb1 = null;         //Usados para que en los ComboBox NO se repitan valores ya elegidos
    private Envase cb2 = null;
    private Envase cb3 = null;
    private Envase elegido = null;

    /**
     * Initializes the controller class.
     */
    public void initComponentes() {
        imagenFondo.setImage(new Image("/interfaz/Imagenes/plantillaEmpresa.png"));

        cbEnvase1.getItems().add(null);
        cbEnvase2.getItems().add(null);
        cbEnvase3.getItems().add(null);

        Iterator<Envase> itEnvases = sistema.getEnvases().iterator();
        while (itEnvases.hasNext()) {
            Envase e = itEnvases.next();
            cbEnvase1.getItems().add(e);
            cbEnvase2.getItems().add(e);
            cbEnvase3.getItems().add(e);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void btnListoClicked(ActionEvent event) throws IOException {
        String nombre = tnombre.getText();
        String material = tmaterial.getText();
        String origen = torigen.getText();
        String precioText = tprecio.getText();

        boolean esComida = (rbComida.isSelected());// ? true : false;      

        //Agregamos los envases seleccionados
        ArrayList<Envase> envasesSugeridos = new ArrayList();
        if (cb1 != null) {
            envasesSugeridos.add(cb1);
        }
        if (cb2 != null) {
            envasesSugeridos.add(cb2);
        }
        if (cb3 != null) {
            envasesSugeridos.add(cb3);
        }

        if ((sistema.esUnNumero(precioText) && Integer.parseInt(precioText) > 0) && (sistema.esUnNumero(precioText) && Integer.parseInt(precioText) <= 50000) && !nombre.isEmpty() && !material.isEmpty() && !origen.isEmpty() && envasesSugeridos.size() > 0 && (rbComida.isSelected() || rbBebida.isSelected())) {
            Producto prod;
            prod = new Producto(nombre, material, origen, Integer.parseInt(precioText), esComida, "/interfaz/Imagenes/cajaDefecto.png");
            prod.setEnvasesSugeridos(envasesSugeridos);
            sistema.agregarProducto(prod);
            this.manejador.ventanaDeAviso("AVISO", "Producto agregado exitosamente.", "El producto " + prod.getNombre() + " fue agregado con éxito.");
            this.manejador.cambiarVentanaProductos();
        } else {
            this.manejador.mostrarError(cajaError, txtTitulo, txtDescr, "ERROR", "Ningún campo puede estar vacío y el precio debe ser un número entre 1 y 50000.");
        }
    }

    @FXML
    private void botonCancelarAccion(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaProductos();
    }

    @FXML
    private void cbEnvase1(ActionEvent event) {
        elegido = cbEnvase1.getValue();

        if (elegido != null) {
            cbEnvase2.getItems().remove(elegido);
            cbEnvase3.getItems().remove(elegido);
        } else {
            if (cb1 != null) {
                cbEnvase2.getItems().add(cb1);
                cbEnvase3.getItems().add(cb1);
            }
        }
        cb1 = elegido;
    }

    @FXML
    private void cbEnvase2(ActionEvent event) {
        elegido = cbEnvase2.getValue();

        if (elegido != null) {
            cbEnvase1.getItems().remove(elegido);
            cbEnvase3.getItems().remove(elegido);
        } else {
            if (cb2 != null) {
                cbEnvase1.getItems().add(cb2);
                cbEnvase3.getItems().add(cb2);
            }
        }
        cb2 = elegido;
    }

    @FXML
    private void cbEnvase3(ActionEvent event) {
        elegido = cbEnvase3.getValue();

        if (elegido != null) {
            cbEnvase1.getItems().remove(elegido);
            cbEnvase2.getItems().remove(elegido);
        } else {
            if (cb3 != null) {
                cbEnvase1.getItems().add(cb3);
                cbEnvase2.getItems().add(cb3);
            }
        }
        cb3 = elegido;
    }

    @FXML
    private void botonEstadisticasClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaEstadistica();
    }

    @FXML
    private void botonProductosAccion(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaProductos();
    }

    @FXML
    private void botonMapaClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaMapa();
    }

    @FXML
    private void botonPromoClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaPromos();
    }

    public void setSistema(Sistema s) {
        this.sistema = s;
    }

    public void setManejador(ManejadorVentanas m) {
        this.manejador = m;
    }
}
