package interfaz;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import dominio.*;
import java.io.IOException;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;

public class SeccionEstadisticaEmpresaController implements Initializable {

    @FXML
    private AnchorPane apLienzo;
    @FXML
    private ImageView imgFondo;
    @FXML
    private ScrollPane spPanelPPal;
    @FXML
    private Group grpPanel;
    @FXML
    private GridPane gridEstructuraPanel;
    @FXML
    private Text txtDato1;
    @FXML
    private Text txtResp1;
    @FXML
    private Text txtDato2;
    @FXML
    private Text txtResp2;
    @FXML
    private Text txtDato3;
    @FXML
    private Text txtResp3;
    @FXML
    private Text txtResp5;
    @FXML
    private Text txtDato5;
    @FXML
    private Text txtResp4;
    @FXML
    private Text txtDato6;
    @FXML
    private Button botonEstadisticas;
    @FXML
    private Button botonProductos;
    @FXML
    private Button botonMapa;
    @FXML
    private Button botonPromo;
    @FXML
    private TextField tfMesIngresado;
    @FXML
    private Text txtResp6Cant;
    @FXML
    private Text txtResp6Dinero;
    @FXML
    private Text txtDato41;

    private Sistema sistema;
    private ManejadorVentanas manejador;

    /**
     * Initializes the controller class.
     */
    public void initComponentes() {
        imgFondo.setImage(new Image("/interfaz/Imagenes/plantillaEmpresa.png"));
        txtResp1.setText(sistema.cantEnvasesReutilizados() + "");
        txtResp2.setText(sistema.cantTotalProductosVendidos() + "");
        txtResp3.setText(sistema.getVentas().size() + "");
        txtResp4.setText(sistema.productoMasVendido().getNombre());
        txtResp5.setText(sistema.envaseMasReutilizado() + "");
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO    
    }

    @FXML
    private void botonEstadisticasClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaEstadistica();
    }

    @FXML
    private void botonMapaClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaMapa();
    }

    @FXML
    private void botonPromoClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaPromos();
    }

    private void botonCarritoAccion(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaCarrito();
    }

    @FXML
    private void botonProductos(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaProductos();
    }

    public void setSistema(Sistema s) {
        this.sistema = s;
    }

    public void setManejador(ManejadorVentanas m) {
        this.manejador = m;
    }

    @FXML
    private void tfIngresarMesAccion(ActionEvent event) {
        String mesTexto = tfMesIngresado.getText();
        if (sistema.esUnNumero(mesTexto)) {
            int mesNum = Integer.parseInt(mesTexto);
            if (mesNum <= 12 && mesNum >= 1) {
                txtResp6Cant.setText(sistema.cantVentasEnMes(mesNum) + "");
                txtResp6Dinero.setText(sistema.cantRecaudadaEnMes(mesNum) + "");
            } else {
                txtResp6Cant.setText("Ingrese MES VÁLIDO");
            }
        } else {
            txtResp6Cant.setText("Ingrese MES VÁLIDO");
        }
    }
}
