package interfaz;

import dominio.Producto;
import dominio.Promocion;
import dominio.Sistema;
import dominio.Venta;
import java.io.IOException;
import java.util.Optional;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ManejadorVentanas {

    //ATRIBUTOS
    private Stage stage;
    private Sistema sistema;
    private boolean esCliente;

    ManejadorVentanas(Stage stage, Sistema s) {
        this.setStage(stage);
        this.setSistema(s);
        this.setEsCliente(true);
    }

    public void mostrarError(VBox cajaErrores, Text txtTitulo, Text txtDescripcion, String titulo, String descripcion) {
        cajaErrores.setBackground(new Background(new BackgroundFill(Color.rgb(255, 201, 201), CornerRadii.EMPTY, Insets.EMPTY)));
        txtTitulo.setText(titulo);
        txtDescripcion.setText(descripcion);
    }

    public void ventanaDeError(String titulo, String cabezal, String descripcion) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(titulo);
        alert.setHeaderText(cabezal);
        alert.setContentText(descripcion);
        alert.showAndWait();
    }

    public void ventanaDeAviso(String titulo, String cabezal, String descripcion) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(titulo);
        alert.setHeaderText(cabezal);
        alert.setContentText(descripcion);
        alert.showAndWait();
    }

    public boolean ventanaDeConfirmacion(String titulo, String cabezal, String descripcion) {
        boolean retorno = false;
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(titulo);
        alert.setHeaderText(cabezal);
        alert.setContentText(descripcion);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            retorno = true;
        } else {
            retorno = false;
        }

        return retorno;
    }

    public void cambiarVentanaEstadistica() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        Parent root;
        Scene scene;

        if (this.getEsCliente()) {
            loader.setLocation(getClass().getResource("/interfaz/SeccionEstadistica.fxml"));
            root = loader.load();
            scene = new Scene(root);
            SeccionEstadisticaController ventanaEstadistica = loader.getController();

            this.getStage().setScene(scene);

            ventanaEstadistica.setSistema(this.getSistema());
            ventanaEstadistica.setManejador(this);

            ventanaEstadistica.initComponentes();
        } else {
            loader.setLocation(getClass().getResource("/interfaz/SeccionEstadisticaEmpresa.fxml"));
            root = loader.load();
            scene = new Scene(root);
            SeccionEstadisticaEmpresaController ventanaEstadistica = loader.getController();

            this.getStage().setScene(scene);

            ventanaEstadistica.setSistema(this.getSistema());
            ventanaEstadistica.setManejador(this);
            ventanaEstadistica.initComponentes();
        }
        stage.show();
    }

    public void cambiarVentanaPromos() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        Parent root;
        Scene scene;

        if (this.getEsCliente()) {
            loader.setLocation(getClass().getResource("/interfaz/Promociones.fxml"));
            root = loader.load();
            scene = new Scene(root);

            PromocionesController ventanaPromos = loader.getController();
            this.getStage().setScene(scene);

            ventanaPromos.setSistema(this.getSistema());
            ventanaPromos.setManejador(this);
            ventanaPromos.initComponentes();
        } else {

            loader.setLocation(getClass().getResource("/interfaz/PromocionesEmpresa.fxml"));
            root = loader.load();
            scene = new Scene(root);

            PromocionesEmpresaController ventanaPromoEmpresa = loader.getController();
            this.getStage().setScene(scene);

            ventanaPromoEmpresa.setSistema(this.getSistema());
            ventanaPromoEmpresa.setManejador(this);
            ventanaPromoEmpresa.initComponents();
        }

        stage.show();
    }

    public void cambiarVentanaAgregarPromo() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/interfaz/AgregarPromo.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);

        AgregarPromoController ventanaPromosEmpresa = loader.getController();
        this.getStage().setScene(scene);

        ventanaPromosEmpresa.setearSistema(this.getSistema());
        ventanaPromosEmpresa.setearManejador(this);
        ventanaPromosEmpresa.initComponentes();

        stage.show();
    }

    public void cambiarVentanaPromoSola(Promocion p) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        Parent root;
        Scene scene;

        if (this.getEsCliente()) {
            loader.setLocation(getClass().getResource("/interfaz/PromoSola.fxml"));
            root = loader.load();
            scene = new Scene(root);

            PromoSolaController ventanaPromo = loader.getController();
            this.getStage().setScene(scene);

            ventanaPromo.setSistema(this.getSistema());
            ventanaPromo.setPromo(p);
            ventanaPromo.setManejador(this);
            ventanaPromo.initComponentes();
        } else {
            loader.setLocation(getClass().getResource("/interfaz/PromoSolaEmpresa.fxml"));
            root = loader.load();
            scene = new Scene(root);

            PromoSolaEmpresaController ventanaPromo = loader.getController();
            this.getStage().setScene(scene);

            ventanaPromo.setSistema(this.getSistema());
            ventanaPromo.setPromo(p);
            ventanaPromo.setManejador(this);
            ventanaPromo.initComponentes();

        }

        stage.show();
    }

    public void cambiarVentanaAgregarProducto() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/interfaz/AgregarProducto.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);

        AgregarProductoController ventanaProductosEmpresa = loader.getController();
        this.getStage().setScene(scene);

        ventanaProductosEmpresa.setSistema(this.getSistema());
        ventanaProductosEmpresa.setManejador(this);
        ventanaProductosEmpresa.initComponentes();

        stage.show();
    }

    public void cambiarVentanaUnProdEmpresa(Producto p) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/interfaz/SeccionUnProdEmpresa.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);

        SeccionUnProdEmpresaController ventanaProductosEmpresa = loader.getController();
        this.getStage().setScene(scene);

        ventanaProductosEmpresa.setProducto(p);
        ventanaProductosEmpresa.setManejador(this);
        ventanaProductosEmpresa.setSistema(this.getSistema());
        ventanaProductosEmpresa.initComponentes();

        stage.show();
    }

    public void cambiarVentanaProductos() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        Parent root;
        Scene scene;

        if (this.getEsCliente()) {
            loader.setLocation(getClass().getResource("/interfaz/Productos.fxml"));
            root = loader.load();
            scene = new Scene(root);

            ProductosController ventanaProductos = loader.getController();
            this.getStage().setScene(scene);

            ventanaProductos.setSistema(this.getSistema());
            ventanaProductos.setManejador(this);
            ventanaProductos.initComponentes();
        } else {
            loader.setLocation(getClass().getResource("/interfaz/SeccionProductosEmpresa.fxml"));
            root = loader.load();
            scene = new Scene(root);

            SeccionProductosEmpresaController ventanaProductosEmpresa = loader.getController();
            this.getStage().setScene(scene);

            ventanaProductosEmpresa.setSistema(this.getSistema());
            ventanaProductosEmpresa.setManejador(this);
            ventanaProductosEmpresa.initComponentes();
        }

        stage.show();
    }

    public void cambiarVentanaProductoSolo(Producto p) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        Parent root;
        Scene scene;

        if (this.getEsCliente()) {
            loader.setLocation(getClass().getResource("/interfaz/ProductoSolo.fxml"));
            root = loader.load();
            scene = new Scene(root);

            ProductoSoloController ventanaProductoSolo = loader.getController();
            this.getStage().setScene(scene);

            ventanaProductoSolo.setProducto(p);
            ventanaProductoSolo.setSistema(this.getSistema());
            ventanaProductoSolo.setManejador(this);
            ventanaProductoSolo.initComponentes();
        } else {
            loader.setLocation(getClass().getResource("/interfaz/SeccionUnProdEmpresa.fxml"));
            root = loader.load();
            scene = new Scene(root);

            SeccionUnProdEmpresaController ventanaProductoSoloEmpresa = loader.getController();
            this.getStage().setScene(scene);

            ventanaProductoSoloEmpresa.setProducto(p);
            ventanaProductoSoloEmpresa.setSistema(this.getSistema());
            ventanaProductoSoloEmpresa.setManejador(this);
            ventanaProductoSoloEmpresa.initComponentes();
        }

        stage.show();
    }

    public void cambiarVentanaMapa() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/interfaz/SeccionMapa.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);

        SeccionMapaController ventanaMapa = loader.getController();
        this.getStage().setScene(scene);
        ventanaMapa.setManejador(this);
        ventanaMapa.setSistema(this.getSistema());
        ventanaMapa.initComponentes();

        stage.show();
    }

    public void cambiarVentanaCarrito() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/interfaz/SeccionCarrito.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);

        SeccionCarritoController ventanaCarrito = loader.getController();
        this.getStage().setScene(scene);
        ventanaCarrito.setManejador(this);
        ventanaCarrito.setSistema(this.getSistema());
        ventanaCarrito.initComponentes();

        stage.show();
    }

    public void cambiarVentanaComprar() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/interfaz/Comprar.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);

        ComprarController ventanaCarrito = loader.getController();
        this.getStage().setScene(scene);
        ventanaCarrito.setManejador(this);
        ventanaCarrito.setSistema(this.getSistema());
        ventanaCarrito.initComponentes();

        stage.show();
    }

    public void cambiarVentanaETicket(Venta v) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/interfaz/ETicket.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);

        ETicketController ventanaTicket = loader.getController();
        this.getStage().setScene(scene);

        ventanaTicket.setSistema(this.getSistema());
        ventanaTicket.serManejador(this);
        ventanaTicket.setVenta(v);
        ventanaTicket.initComponentes();

        stage.show();
    }

    //GETTERS
    public Stage getStage() {
        return stage;
    }

    public Sistema getSistema() {
        return sistema;
    }

    public boolean getEsCliente() {
        return esCliente;
    }

    //SETTERS
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setSistema(Sistema sistema) {
        this.sistema = sistema;
    }

    public void setEsCliente(boolean esCliente) {
        this.esCliente = esCliente;
    }

}
