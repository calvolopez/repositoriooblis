package interfaz;

import dominio.Envase;
import dominio.Producto;
import dominio.Promocion;
import dominio.Sistema;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import tads.Asociacion;

public class SeccionCarritoController implements Initializable {

    @FXML
    private AnchorPane apLienzo;
    @FXML
    private ImageView imgFondo;
    @FXML
    private ScrollPane spPanelPPal;
    @FXML
    private Group grpPanel;
    @FXML
    private ImageView imgFondoPanel;
    @FXML
    private GridPane gridEstructuraPanel;
    @FXML
    private Text txtTitulo;
    @FXML
    private Text txtCantKG;
    @FXML
    private Text txtDatoKG;
    @FXML
    private Text txtDatoL;
    @FXML
    private Text txtCantL;
    @FXML
    private Text txtDatoCostoCarrito;
    @FXML
    private Text txtDatoCosto;
    @FXML
    private Button btnComprar;
    @FXML
    private Text txtFooter;
    @FXML
    private Button botonEstadisticas;
    @FXML
    private Button botonProductos;
    @FXML
    private Button botonMapa;
    @FXML
    private Button botonPromo;
    @FXML
    private Button botonCarro;
    @FXML
    private AnchorPane apFondoProductos;
    @FXML
    private Button btnDescartarCarrito;
    @FXML
    private Text txtProducto1;
    @FXML
    private Text txtCantidad1;
    @FXML
    private Text txtCosto1;
    @FXML
    private Text txtCarrito;

    private Sistema sistema;
    private GridPane tablaProductos;
    private ManejadorVentanas manejador;

    /**
     * Initializes the controller class.
     */
    public void initComponentes() {
        imgFondo.setImage(new Image("/interfaz/Imagenes/plantillaCliente.png"));
        this.imprimirElCarrito();
        txtDatoKG.setText(sistema.getCarrito().cantKilos() + "");
        txtDatoL.setText(sistema.getCarrito().cantLitros() + "");
        txtDatoCosto.setText("$ " + sistema.getCarrito().getCosto());

        if (sistema.getCarrito().getProductos().isEmpty() && sistema.getCarrito().getPromos().isEmpty()) {
            btnComprar.setDisable(true);
            btnDescartarCarrito.setDisable(true);
        } else {
            btnComprar.setDisable(false);
            btnDescartarCarrito.setDisable(false);
        }
        this.txtCarrito.setText(sistema.getCarrito().getProductos().size() + sistema.getCarrito().getPromos().size() + "");
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void accionClickComprar(ActionEvent event) throws IOException {
        if (sistema.getCarrito().getProductos().size() == 0 && sistema.getCarrito().getPromos().size() == 0) {
            this.manejador.ventanaDeError("ERROR", "Carrito vacio", "Su carrito no contiene elementos. Agregue.");
        } else {
            this.manejador.cambiarVentanaComprar();
        }
    }

    @FXML
    private void botonDescartarAaccion(ActionEvent event) throws IOException {
        if (this.manejador.ventanaDeConfirmacion("ATENCIÓN", "Confirme que desea descartar su carrito", "Si selecciona 'OK', se perderá toda la información de su carrito.")) {
            this.sistema.vaciarCarrito();
        }
        this.manejador.cambiarVentanaCarrito();
    }

    @FXML
    private void botonEstadisticasClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaEstadistica();
    }

    @FXML
    private void botonMapaClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaMapa();
    }

    @FXML
    private void botonPromoClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaPromos();
    }

    @FXML
    private void botonCarritoAccion(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaCarrito();
    }

    @FXML
    private void botonProductosAccion(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaProductos();
    }

    public void imprimirElCarrito() {
        GridPane tablaExterna = new GridPane();
        tablaExterna.setPrefWidth(335);
        tablaExterna.setMaxWidth(335);
        tablaExterna.setMinWidth(335);

        apFondoProductos.getChildren().add(tablaExterna);

        ArrayList<Asociacion<Asociacion<Producto, Integer>, Envase>> lista = sistema.getCarrito().getProductos();
        ArrayList<Asociacion<Promocion, Integer>> listaPromos = sistema.getCarrito().getPromos();

        for (int i = 0; i < lista.size(); i++) {
            Producto prod = lista.get(i).getDominio().getDominio();
            int cant = lista.get(i).getDominio().getRango();

            GridPane tabla = new GridPane();            //Cada Casilla tiene una tabla (Prod / Cant / PrecioX1)
            tabla.setMaxWidth(335);
            tabla.setPrefSize(335, 140);
            tabla.setMaxSize(335, 140);

            if (i % 2 == 0) {
                tabla.setBackground(new Background(new BackgroundFill(Color.rgb(229, 229, 229), CornerRadii.EMPTY, Insets.EMPTY)));
            }

            //Creamos los Componentes a Agregar
            VBox fotoYNombre = new VBox();  //Cada casilla tiene un VBox para la foto y el nombre del prod
            VBox margen1 = new VBox();
            Text cantidad = new Text();
            Text precio = new Text();

            cantidad.setText("x" + cant);                //Setteamos la Cant
            precio.setText("$ " + prod.getPrecio());    //Setteamos el Precio

            fotoYNombre.setMaxSize(220, 140);            //Settearemos el Producto
            fotoYNombre.setMinSize(220, 140);
            margen1.setMaxHeight(10);            //Settearemos el Producto
            margen1.setMinHeight(10);

            ImageView imagen = new ImageView(new Image(prod.getUrlImagen()));   //Setteamos la Imagen
            imagen.setPreserveRatio(true);
            imagen.setLayoutY(10);
            imagen.setFitHeight(90);

            Text txt = new Text(prod.getNombre());                              //Setteamos el Nombre
            txt.setLayoutY(185);
            txt.setLayoutX(15);
            txt.setTextAlignment(TextAlignment.CENTER);

            fotoYNombre.getChildren().add(txt);      //Agregamos el Texto a su A.P.
            fotoYNombre.getChildren().add(margen1);
            fotoYNombre.getChildren().add(imagen);   //Agregamos la Imagen a su A.P.

            tabla.getColumnConstraints().add(new ColumnConstraints(10));    //Esta columna es solo para margen
            tabla.getColumnConstraints().add(new ColumnConstraints(200));   //column 0 is 205 wide
            tabla.getColumnConstraints().add(new ColumnConstraints(60));
            tabla.getColumnConstraints().add(new ColumnConstraints(60));
            tabla.getColumnConstraints().add(new ColumnConstraints(5));     //Margen

            //Agregamos cada elemento a la columna que merezca
            tabla.add(fotoYNombre, 1, 0);
            tabla.add(cantidad, 2, 0);
            tabla.add(precio, 3, 0);

            GridPane.setValignment(cantidad, VPos.TOP);
            GridPane.setValignment(precio, VPos.TOP);
            GridPane.setHalignment(cantidad, HPos.CENTER);
            GridPane.setHalignment(precio, HPos.CENTER);

            tablaExterna.add(tabla, 0, i);
        }

        int ultimoColor = lista.size() % 2;
        int cantProds = lista.size();
        for (int i = 0; i < listaPromos.size(); i++) {
            Promocion promo = listaPromos.get(i).getDominio();
            int cant = listaPromos.get(i).getRango();

            GridPane tabla = new GridPane();            //Cada Casilla tiene una tabla (Prod / Cant / PrecioX1)
            tabla.setMaxWidth(335);
            tabla.setPrefSize(335, 140);
            tabla.setMaxSize(335, 140);

            if ((i + ultimoColor) % 2 == 0) {
                tabla.setBackground(new Background(new BackgroundFill(Color.rgb(229, 229, 229), CornerRadii.EMPTY, Insets.EMPTY)));
            }

            //Creamos los Componentes a Agregar
            VBox fotoYNombre = new VBox();  //Cada casilla tiene un VBox para la foto y el nombre del prod
            VBox margen1 = new VBox();
            Text cantidad = new Text();
            Text precio = new Text();

            cantidad.setText("x" + cant);                //Setteamos la Cant
            precio.setText("$ " + promo.getPrecio());    //Setteamos el Precio

            fotoYNombre.setMaxSize(220, 140);            //Settearemos el Producto
            fotoYNombre.setMinSize(220, 140);
            margen1.setMaxHeight(10);            //Settearemos el Producto
            margen1.setMinHeight(10);

            ImageView imagen = new ImageView(new Image(promo.getProducto().getUrlImagen()));   //Setteamos la Imagen
            imagen.setPreserveRatio(true);
            imagen.setLayoutY(10);
            imagen.setFitHeight(90);

            Text txt;
            if (promo.getEsDescuento()) {
                txt = new Text("%" + promo.getValor() + " en '" + promo.getProducto().getNombre() + "'"); //Setteamos el Nombre

            } else {
                txt = new Text(promo.getValor() + "x" + promo.getValor2() + " en '" + promo.getProducto().getNombre() + "'");
            }
            txt.setLayoutY(185);
            txt.setLayoutX(15);
            txt.setTextAlignment(TextAlignment.CENTER);

            fotoYNombre.getChildren().add(txt);      //Agregamos el Texto a su A.P.
            fotoYNombre.getChildren().add(margen1);
            fotoYNombre.getChildren().add(imagen);   //Agregamos la Imagen a su A.P.

            tabla.getColumnConstraints().add(new ColumnConstraints(10));    //Esta columna es solo para margen
            tabla.getColumnConstraints().add(new ColumnConstraints(200));   //column 0 is 205 wide
            tabla.getColumnConstraints().add(new ColumnConstraints(60));
            tabla.getColumnConstraints().add(new ColumnConstraints(60));
            tabla.getColumnConstraints().add(new ColumnConstraints(5));     //Margen

            //Agregamos cada elemento a la columna que merezca
            tabla.add(fotoYNombre, 1, 0);
            tabla.add(cantidad, 2, 0);
            tabla.add(precio, 3, 0);

            GridPane.setValignment(cantidad, VPos.TOP);
            GridPane.setValignment(precio, VPos.TOP);
            GridPane.setHalignment(cantidad, HPos.CENTER);
            GridPane.setHalignment(precio, HPos.CENTER);

            tablaExterna.add(tabla, 0, cantProds + i);
        }
    }

    public void setSistema(Sistema sistema) {
        this.sistema = sistema;
    }

    public void setManejador(ManejadorVentanas m) {
        this.manejador = m;
    }
}
