package interfaz;

import dominio.Envase;
import dominio.Producto;
import dominio.Sistema;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class SeccionUnProdEmpresaController implements Initializable {

    @FXML
    private ImageView imagenFondo;
    @FXML
    private Text txtNombreProd;
    @FXML
    private Text txtMateriales;
    @FXML
    private Text txtOrigenMateriales;
    @FXML
    private ImageView imgImagenProd;
    @FXML
    private Text txtCodigoProducto;
    @FXML
    private VBox vbCajaEnvases;
    @FXML
    private TextField tnombre1;
    @FXML
    private TextField tmaterial;
    @FXML
    private TextField tprecio;
    @FXML
    private Button btnModNombre;
    @FXML
    private Button btnModMaterial;
    @FXML
    private Button btnModPrecio;
    @FXML
    private Button btnEliminarProducto;
    @FXML
    private TextArea torigen;
    @FXML
    private Button btnModOrigen;
    @FXML
    private Button botonEstadisticas;
    @FXML
    private Button botonProductos;
    @FXML
    private Button botonMapa;
    @FXML
    private Button botonPromo;
    @FXML
    private Text txtDatoPrecio;
    @FXML
    private VBox cajaError;
    @FXML
    private Text txtTituloError;
    @FXML
    private Text txtDescrError;

    private ManejadorVentanas manejador;
    private Sistema sistema;
    private Producto producto;

    /**
     * Initializes the controller class.
     */
    public void initComponentes() {
        imagenFondo.setImage(new Image("/interfaz/Imagenes/plantillaEmpresa.png"));

        imprimirEnvasesSugeridos();

        imgImagenProd.setImage(new Image(producto.getUrlImagen()));
        txtCodigoProducto.setText(producto.getCodigo() + "");
        txtOrigenMateriales.setText(producto.getOrigenMateriaPrima());
        txtMateriales.setText(producto.getMaterial());
        txtNombreProd.setText(producto.getNombre());
        txtDatoPrecio.setText("$" + producto.getPrecio());

        btnModNombre.setShape(new Circle(1.5));
        btnModPrecio.setShape(new Circle(1.5));
        btnModOrigen.setShape(new Circle(1.5));
        btnModMaterial.setShape(new Circle(1.5));
    }

    private void imprimirEnvasesSugeridos() {
        GridPane tablaExterna = new GridPane();
        tablaExterna.setPrefWidth(334);
        tablaExterna.setMaxWidth(334);
        tablaExterna.setMinWidth(334);

        vbCajaEnvases.getChildren().add(tablaExterna);

        ArrayList<Envase> lista = this.producto.getEnvasesSugeridos();
        for (int i = 0; i < lista.size(); i++) {
            Envase env = lista.get(i);
            String nombre = env.getNombre();

            GridPane tabla = new GridPane();            //Cada Casilla tiene una tabla (Prod / Cant / PrecioX1)
            tabla.setMaxWidth(334);
            tabla.setPrefSize(334, 30);
            tabla.setMaxSize(334, 40);

            //Creamos los Componentes a Agregar
            Text titulo = new Text((1 + i) + " - " + nombre);
            titulo.setTextAlignment(TextAlignment.CENTER);

            tabla.getColumnConstraints().add(new ColumnConstraints(15));    //Marge
            tabla.getColumnConstraints().add(new ColumnConstraints(300));   //Contenido (column 0 is 300 wide)
            tabla.getColumnConstraints().add(new ColumnConstraints(20));    //Margen

            //Agregamos cada elemento a la columna que merezca
            tabla.add(titulo, 1, 0);

            GridPane.setValignment(titulo, VPos.CENTER);
            GridPane.setHalignment(titulo, HPos.LEFT);

            tablaExterna.add(tabla, 0, i);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void btnNombreAccion(ActionEvent event) throws IOException {
        if (!this.tnombre1.getText().isEmpty()) {
            sistema.getProductos().get(sistema.getProductos().indexOf(producto)).setNombre(tnombre1.getText()); //Modificamos el nombre del producto
            this.manejador.ventanaDeAviso("ATENCIÓN", "Dato modificado correctamente", "Se cambió el nombre del producto '" + producto.getNombre() + "'.");
            this.manejador.cambiarVentanaProductoSolo(producto);
        } else {
            this.manejador.mostrarError(cajaError, txtTituloError, txtDescrError, "ERROR", "El campo 'nombre' debe tener contenido para actualizar el nombre del producto.");
        }
    }

    @FXML
    private void btnMaterialAccion(ActionEvent event) throws IOException {
        if (!this.tmaterial.getText().isEmpty()) {
            sistema.getProductos().get(sistema.getProductos().indexOf(producto)).setMaterial(tmaterial.getText()); //Modificamos el material del producto
            this.manejador.ventanaDeAviso("ATENCIÓN", "Dato modificado correctamente", "Se cambió el material del producto '" + producto.getNombre() + "'.");
            this.manejador.cambiarVentanaProductoSolo(producto);
        } else {
            this.manejador.mostrarError(cajaError, txtTituloError, txtDescrError, "ERROR", "El campo 'material' debe tener contenido para actualizar el material del producto.");
        }
    }

    @FXML
    private void btnPrecioAccion(ActionEvent event) throws IOException {
        String precioTexto = this.tprecio.getText();
        if (!precioTexto.isEmpty() && sistema.esUnNumero(precioTexto)) //Si no es vacio y es numero
        {
            sistema.getProductos().get(sistema.getProductos().indexOf(producto)).setPrecio(Integer.parseInt(precioTexto)); //Modificamos el material del producto
            this.manejador.ventanaDeAviso("ATENCIÓN", "Dato modificado correctamente", "Se cambió el precio del producto '" + producto.getNombre() + "'.");
            this.manejador.cambiarVentanaProductoSolo(producto);
        } else if (!sistema.esUnNumero(precioTexto)) {
            this.manejador.mostrarError(cajaError, txtTituloError, txtDescrError, "ERROR", "El precio debe ser un valor numerico.");
        }
    }

    @FXML
    private void btnEliminar(ActionEvent event) throws IOException {
        if (this.manejador.ventanaDeConfirmacion("ATENCIÓN", "Confirmación de eliminación de producto", "Confirme si desea eliminar el producto " + this.producto.getNombre() + ".")) {
            this.sistema.eliminarProducto(producto);
            this.manejador.cambiarVentanaProductos();
        } else {
            this.manejador.cambiarVentanaUnProdEmpresa(producto);
        }
    }

    @FXML
    private void btnOrigenAccion(ActionEvent event) throws IOException {
        if (!this.torigen.getText().isEmpty()) {
            sistema.getProductos().get(sistema.getProductos().indexOf(producto)).setOrigenMateriaPrima(this.torigen.getText()); //Modificamos el material del producto
            this.manejador.ventanaDeAviso("ATENCIÓN", "Dato modificado correctamente", "Se cambió el origen del producto '" + producto.getNombre() + "'.");
            this.manejador.cambiarVentanaProductoSolo(producto);
        } else {
            this.manejador.mostrarError(cajaError, txtTituloError, txtDescrError, "ERROR", "El campo 'origen' debe tener contenido para actualizar el origen de la materia del producto");
        }
    }

    @FXML
    private void botonEstadisticasClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaEstadistica();
    }

    @FXML
    private void botonProductosClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaProductos();
    }

    @FXML
    private void botonMapaClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaMapa();
    }

    @FXML
    private void botonPromoClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaPromos();
    }

    public void setSistema(Sistema s) {
        this.sistema = s;
    }

    public void setProducto(Producto p) {
        this.producto = p;
    }

    public void setManejador(ManejadorVentanas m) {
        this.manejador = m;
    }
}
