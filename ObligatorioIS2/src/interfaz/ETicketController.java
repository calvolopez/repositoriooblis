package interfaz;

import dominio.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.scene.control.Button;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import tads.Asociacion;

public class ETicketController implements Initializable {

    @FXML
    private Text tfecha;
    @FXML
    private Text tcosto;
    @FXML
    private Button botnoOK;
    @FXML
    private Text tfechaVencimiento;
    @FXML
    private Text txtIdCompra;
    @FXML
    private Text tnombreCliente;
    @FXML
    private VBox cajaDescripcionProds;
    @FXML
    private Text txtCodigoRetiro;

    private Venta venta;
    private Sistema sistema;
    private ManejadorVentanas manejador;

    /**
     * Initializes the controller class.
     */
    public void initComponentes() {
        tfecha.setText(venta.getFechaVenta().getDay() + "/" + venta.getFechaVenta().getMonth() + "/" + venta.getFechaVenta().getYear());

        txtCodigoRetiro.setText("CV" + 10 + venta.getCodigo() * 2 + 546);
        tcosto.setText("$ " + venta.getCosto());
        tfechaVencimiento.setText(venta.getFechaVenta().getDay() + "/" + venta.getFechaVenta().getMonth() + "/" + (venta.getFechaVenta().getYear() + 1));
        tnombreCliente.setText(venta.getCliente().getNombre() + " " + venta.getCliente().getApellido());
        txtIdCompra.setText(venta.getCodigo() + "");

        this.imprimirProductos();
    }

    public void imprimirProductos() {
        //Agregamos los productos vendidos
        ArrayList<Asociacion<Asociacion<Producto, Integer>, Envase>> lista = venta.getProductos();
        ArrayList<Asociacion<Promocion, Integer>> listaPromos = venta.getPromos();

        GridPane tabla = new GridPane();
        tabla.setPrefSize(335, 300);
        tabla.setMaxWidth(335);
        tabla.setVgap(3);
        cajaDescripcionProds.setSpacing(15);

        tabla.setLayoutX(0);
        tabla.setLayoutY(0);

        //Setteamos el ancho de las columnas
        tabla.getColumnConstraints().add(new ColumnConstraints(7)); // column 0 is 205 wide
        tabla.getColumnConstraints().add(new ColumnConstraints(100));
        tabla.getColumnConstraints().add(new ColumnConstraints(65));
        tabla.getColumnConstraints().add(new ColumnConstraints(70));
        tabla.getColumnConstraints().add(new ColumnConstraints(72));
        tabla.getColumnConstraints().add(new ColumnConstraints(13));
        tabla.getColumnConstraints().get(1).setHalignment(HPos.LEFT);
        tabla.getColumnConstraints().get(2).setHalignment(HPos.RIGHT);
        tabla.getColumnConstraints().get(3).setHalignment(HPos.RIGHT);
        tabla.getColumnConstraints().get(4).setHalignment(HPos.RIGHT);

        cajaDescripcionProds.getChildren().add(tabla);

        for (int i = 0; i < lista.size(); i++) {
            Text nombre = new Text(lista.get(i).getDominio().getDominio().getNombre());
            Text cant = new Text("" + lista.get(i).getDominio().getRango());
            Text precio = new Text(lista.get(i).getDominio().getDominio().getPrecio() + "");
            Text total = new Text(lista.get(i).getDominio().getDominio().getPrecio() * lista.get(i).getDominio().getRango() + "");

            tabla.add(nombre, 1, i);
            tabla.add(cant, 2, i);
            tabla.add(precio, 3, i);
            tabla.add(total, 4, i);
        }
        int cantImpresos = lista.size();
        for (int i = 0; i < listaPromos.size(); i++) {
            Promocion promo = listaPromos.get(i).getDominio();

            Text nombre;
            if (promo.getEsDescuento()) {
                nombre = new Text("%" + promo.getValor() + " en '" + promo.getProducto().getNombre() + "'"); //Setteamos el Nombre

            } else {
                nombre = new Text(promo.getValor() + "x" + promo.getValor2() + " en '" + promo.getProducto().getNombre() + "'");
            }
            nombre.setWrappingWidth(110);

            Text cant = new Text("" + listaPromos.get(i).getRango());

            Text precio = new Text(promo.getPrecio() + "");
            Text total = new Text(promo.getPrecio() * listaPromos.get(i).getRango() + "");

            tabla.add(nombre, 1, cantImpresos + i);
            tabla.add(cant, 2, cantImpresos + i);
            tabla.add(precio, 3, cantImpresos + i);
            tabla.add(total, 4, cantImpresos + i);
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void botonOKClicked(ActionEvent event) throws IOException {
        //AVISAR QUE SE ENVIO LA FACTURA AL MAIL
        this.sistema.vaciarCarrito();
        this.manejador.cambiarVentanaProductos();
    }

    public void setSistema(Sistema s) {
        this.sistema = s;
    }

    public void serManejador(ManejadorVentanas m) {
        this.manejador = m;
    }

    public void setVenta(Venta v) {
        this.venta = v;
    }

}
