package interfaz;

import dominio.Envase;
import dominio.Sistema;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

public class SeccionEstadisticaController implements Initializable {

    @FXML
    private AnchorPane apLienzo;
    @FXML
    private ImageView imgFondo;
    @FXML
    private ScrollPane spPanelPPal;
    @FXML
    private Group grpPanel;
    @FXML
    private ImageView imgFondoPanel;
    @FXML
    private GridPane gridEstructuraPanel;
    @FXML
    private Text txtTitulo;
    @FXML
    private AnchorPane apSeccion1;
    @FXML
    private Text txtSeccion1;
    @FXML
    private Text txtDato1;
    @FXML
    private Text txtElegiKGoL;
    @FXML
    private Text txtElegiEnvase;
    @FXML
    private ComboBox<Envase> cbProductos;
    @FXML
    private Text txtCantEnvasesReutilizados;
    @FXML
    private Button botonEstadisticas;
    @FXML
    private Button botonProductos;
    @FXML
    private Button botonMapa;
    @FXML
    private Button botonPromo;
    @FXML
    private Text txtEnvases;
    @FXML
    private Button btnCarrito;
    @FXML
    private Text txtCarrito;
    @FXML
    private Button botonDatoNuevo;
    @FXML
    private Text txtDatoRandom;

    private Sistema sistema;
    private ManejadorVentanas manejador;
    private ArrayList<String> listaDatos = new ArrayList();
    private int contador = 0;

    /**
     * Initializes the controller class.
     */
    //METODOS
    public void initComponentes() {
        imgFondo.setImage(new Image("/interfaz/Imagenes/plantillaCliente.png"));
        txtDato1.setText(sistema.cantEnvasesReutilizados() + "");

        Iterator<Envase> itEnvases = sistema.getEnvases().iterator();
        while (itEnvases.hasNext()) {
            cbProductos.getItems().add(itEnvases.next());
        }

        botonDatoNuevo.setShape(new Circle(1.7));
        this.txtCarrito.setText(sistema.getCarrito().getProductos().size() + sistema.getCarrito().getPromos().size() + "");

        //Agregamos los datos random a las listas
        DecimalFormat df2 = new DecimalFormat("#.##");

        String dato1 = "De le venta de comida, calculando 1 kg de comida por bandeja plástica, prevenimos el uso de " + df2.format(this.sistema.kilosVendidos() * 0.1) + " kg de plástico.";
        String dato2 = "De la venta de bebida, calculando 1 L de bebida por botella plástica, prevenimos el uso de " + df2.format(this.sistema.litrosVendidos() * 0.1) + " kg de plástico.";
        String dato3 = "Con lo ahorrado en plástico por la venta de comida, se podrian haber fabricado " + df2.format(this.sistema.kilosVendidos() * 0.1 / 0.006) + " lapiceras.";
        String dato4 = "Con lo ahorrado en plástico por la venta de bebidas, se podrían haber fabricado " + df2.format(this.sistema.kilosVendidos() * 0.1 / 0.2) + " carcasas de celular.";
        String dato5 = "De la venta de comida, se ahorraron " + df2.format(0.2 * this.sistema.kilosVendidos()) + " metros de bandejas plásticas.";
        listaDatos.add(dato1);
        listaDatos.add(dato2);
        listaDatos.add(dato3);
        listaDatos.add(dato4);
        listaDatos.add(dato5);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void accionCbProductos(ActionEvent event) {
        Envase envase = cbProductos.getValue();
        txtCantEnvasesReutilizados.setText("El envase '" + envase.getNombre() + "' se utilizó " + envase.cantidadDeUsos() + " veces.");
    }

    @FXML
    private void botonEstadisticasClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaEstadistica();
    }

    @FXML
    private void botonMapaClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaMapa();
    }

    @FXML
    private void botonPromoClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaPromos();
    }

    @FXML
    private void botonProductosAccion(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaProductos();
    }

    @FXML
    private void btnCarritoAccion(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaCarrito();
    }

    @FXML
    private void accionClickKG(ActionEvent event) {

        if (contador >= 4) {
            contador = 0;
        }
        txtDatoRandom.setText(listaDatos.get(contador));
        contador++;

    }

    public void setSistema(Sistema sistema) {
        this.sistema = sistema;
    }

    public void setManejador(ManejadorVentanas m) {
        this.manejador = m;
    }

}
