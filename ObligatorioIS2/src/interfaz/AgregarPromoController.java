package interfaz;

import dominio.Producto;
import dominio.Promocion;
import dominio.Sistema;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class AgregarPromoController implements Initializable {

    @FXML
    private Button botonMapa;
    @FXML
    private Button botonPromo;
    @FXML
    private Button botonEstadisticas;
    @FXML
    private Button botonProductos;
    @FXML
    private Button btnAgregar;
    @FXML
    private ComboBox<Producto> cbProductos;
    @FXML
    private RadioButton rbNx1;
    @FXML
    private RadioButton rbDescuento;
    @FXML
    private TextArea tfDescr;
    @FXML
    private ToggleGroup tgTipo;
    @FXML
    private TextField tfCantDesc;
    @FXML
    private TextField tfValor1;
    @FXML
    private TextField tfValor2;
    @FXML
    private VBox cajaError;
    @FXML
    private Text txtTitulo;
    @FXML
    private Text txtDescr;

    private Sistema sistema;
    private ManejadorVentanas manejador;

    /**
     * Initializes the controller class.
     */
    public void initComponentes() {
        //Agregamos los productos al ComboBox 'cbProd'
        Iterator<Producto> itProductos = sistema.getProductos().iterator();
        while (itProductos.hasNext()) {
            cbProductos.getItems().add(itProductos.next());
        }

        tfValor1.setDisable(true);
        tfValor2.setDisable(true);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void botonMapaClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaMapa();
    }

    @FXML
    private void botonPromoClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaPromos();
    }

    @FXML
    private void botonEstadisticasClicked(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaEstadistica();
    }

    @FXML
    private void botonProductosAccion(ActionEvent event) throws IOException {
        this.manejador.cambiarVentanaProductos();
    }

    @FXML
    private void btnAgregarClicked(ActionEvent event) throws IOException {
        Producto elProducto = cbProductos.getValue();
        String descripcion = tfDescr.getText();
        String cantDescTxt = tfCantDesc.getText();
        int valor1 = 0;
        int valor2 = 0;

        if (cbProductos.getValue() != null) {
            if (!descripcion.isEmpty()) {
                if (rbDescuento.isSelected()) {
                    if (sistema.esUnNumero(cantDescTxt) && Integer.parseInt(cantDescTxt) >= 10 && Integer.parseInt(cantDescTxt) <= 65) {
                        valor1 = Integer.parseInt(cantDescTxt);
                        sistema.agregarPromo(new Promocion(elProducto, descripcion, true, valor1, 0));
                        this.manejador.cambiarVentanaPromos();
                    } else {
                        this.manejador.mostrarError(cajaError, txtTitulo, txtDescr, "ERROR", "La cantidad de descuento debe ser un número entre 10 y 65.");
                    }
                } else {
                    if (sistema.esUnNumero(tfValor1.getText()) && sistema.esUnNumero(tfValor2.getText())) {
                        valor1 = Integer.parseInt(tfValor1.getText());
                        valor2 = Integer.parseInt(tfValor2.getText());
                        if (valor1 > 1 && valor1 < 7 && valor2 > 0 && valor2 < valor1) {
                            sistema.agregarPromo(new Promocion(elProducto, descripcion, true, valor1, valor2));
                            this.manejador.cambiarVentanaPromos();
                        } else {
                            this.manejador.mostrarError(cajaError, txtTitulo, txtDescr, "ERROR", "La cantidad de promoción primera debe ser un número entre 2 y 6, mayor a la cantidad segunda.");
                        }
                    } else {
                        this.manejador.mostrarError(cajaError, txtTitulo, txtDescr, "ERROR", "Las cantidades de promoción deben ser números (entre 1 y 6)");
                    }
                }
            } else {
                this.manejador.mostrarError(cajaError, txtTitulo, txtDescr, "ERROR", "La promoción debe tener una descripción.");
            }
        } else {
            this.manejador.mostrarError(cajaError, txtTitulo, txtDescr, "ERROR", "Debe seleccionar un producto para la promoción.");
        }
    }

    @FXML
    private void rbNXMAccion(ActionEvent event) {
        tfCantDesc.setDisable(true);
        tfValor1.setDisable(false);
        tfValor2.setDisable(false);
    }

    @FXML
    private void rbDescuentoAccion(ActionEvent event) {
        tfCantDesc.setDisable(false);
        tfValor1.setDisable(true);
        tfValor2.setDisable(true);
    }

    @FXML
    private void cbProductos(ActionEvent event) {

    }

    public void setearSistema(Sistema s) {
        this.sistema = s;
    }

    public void setearManejador(ManejadorVentanas m) {
        this.manejador = m;
    }
}
