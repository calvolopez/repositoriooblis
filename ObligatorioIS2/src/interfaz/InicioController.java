package interfaz;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class InicioController {

    @FXML
    private AnchorPane apLienzo;
    @FXML
    private ImageView imgFondo;
    @FXML
    private Button btnEmpresa;
    @FXML
    private Button btnCliente;
    @FXML
    private ImageView imgEmpresa;
    @FXML
    private ImageView imgCliente;

    private ManejadorVentanas manejador;

    public void initComponentes() {
        imgFondo.setImage(new Image("/interfaz/Imagenes/plantillaEmpresa.png"));
        imgEmpresa.setImage(new Image("/interfaz/Imagenes/empresa.png"));
        imgCliente.setImage(new Image("/interfaz/Imagenes/user.png"));
    }

    @FXML
    private void btnCliente(ActionEvent event) throws IOException {
        this.manejador.setEsCliente(true);
        this.manejador.cambiarVentanaProductos();

    }

    @FXML
    private void btnEmpresaAccion(ActionEvent event) throws IOException {
        this.manejador.setEsCliente(false);
        this.manejador.cambiarVentanaProductos();
    }

    void setManejador(ManejadorVentanas m) {
        this.manejador = m;
    }

}
