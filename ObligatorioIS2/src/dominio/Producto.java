package dominio;

import java.util.ArrayList;
import dominio.Envase;

public class Producto implements Comparable {

    //ATRIBUTOS
    private static int cantProductos = 0;   //Varible GLOBAL de la clase para llevar el codigo
    private static int[] tablaCantidadVendidos = new int[500];    //Array donde tabla[i] = cantVendidos (para el producto con codigo 'i')
    private String material;
    private String origenMateriaPrima;
    private String nombre;
    private int codigo;                     //Codigo UNICO
    private int precio;                     //Precio Unitario (kg/litro)
    private boolean esComida;
    private ArrayList<Envase> envasesSugeridos;
    private String urlImagen;

    //CONSTRUCTORES
    public Producto(String nombre, String material, String origenMateriaPrima, int precio, boolean esComida, String url) {
        this.setMaterial(material);
        this.setOrigenMateriaPrima(origenMateriaPrima);
        this.setNombre(nombre);
        this.setCodigo(this.getCantProductos() + 1);
        this.setCantProductos(cantProductos + 1);
        this.setPrecio(precio);
        this.setEsComida(esComida);
        this.setEnvasesSugeridos(new ArrayList<Envase>());
        this.setUrlImagen(url);

        tablaCantidadVendidos[this.getCodigo()] = 0;        //Inicializamos el Array en la posicion del producto con '0 ventas'
    }

    public Producto() {
        this.setMaterial("");
        this.setOrigenMateriaPrima("");
        this.setNombre("");
        this.setCodigo(this.getCantProductos() + 1);
        this.setCantProductos(cantProductos + 1);
        this.setPrecio(0);
        this.setEsComida(false);
        this.setEnvasesSugeridos(new ArrayList<Envase>());
        this.setUrlImagen("");

        tablaCantidadVendidos[this.getCodigo()] = 0;
    }

    //PRE: -
    //POS: Agrega el envase 'e' como envase sugerido del producto
    public void agregarEnvaseSugerido(Envase e) {
        this.getEnvasesSugeridos().add(e);
    }

    //PRE: -
    //POS: Elimina el envase 'e' de los envases sugeridos
    public void eliminarEnvaseSugerido(Envase e) {
        this.getEnvasesSugeridos().remove(e);
    }

    //PRE: -
    //POS: Suma 'cantidadNueva' a la cantidad de vendidos de 'this'
    public void agregarCantidadVendidos(int cantidadNueva) {
        int codigoProducto = this.getCodigo();
        if (codigoProducto <= cantProductos) //Si existe el producto
        {
            tablaCantidadVendidos[codigoProducto] += cantidadNueva;
        }
    }

    //PRE: -
    //POS: Retorna la cantidad de veces que se vendio 'this' 
    public int cantidadVendidos() {
        return tablaCantidadVendidos[this.getCodigo()];
    }

    //PRE: -
    //POS: Retorna el codigo del producto mas vendido
    public int productoMasVendido() {
        int max = 0;

        //Buscamos el maximo
        for (int i = 0; i < tablaCantidadVendidos.length; i++) {
            if (tablaCantidadVendidos[i] > max) {
                max = tablaCantidadVendidos[i];
            }
        }

        //Retornamos el codigo del maximo
        for (int i = 0; i < tablaCantidadVendidos.length; i++) {
            if (tablaCantidadVendidos[i] == max) {
                return i;
            }
        }

        return 0;
    }

    //PRE: -
    //POS: Elimina el producto. Ajusta el array static, ajusta cantProd
    public void eliminarProducto(int codigo) {
        this.setCantProductos(cantProductos - 1);

        for (int i = codigo; i < tablaCantidadVendidos.length - 1; i++) {
            tablaCantidadVendidos[i] = tablaCantidadVendidos[i + 1];
        }
    }

    @Override
    public int compareTo(Object o) {
        Producto aux = (Producto) o;

        if (this.codigo > aux.codigo) {
            return 1;
        } else if (this.codigo < aux.codigo) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return this.nombre;
    }

    //GETTER'S
    public String getMaterial() {
        return material;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public String getOrigenMateriaPrima() {
        return origenMateriaPrima;
    }

    public String getNombre() {
        return nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public static int getCantProductos() {
        return cantProductos;
    }

    public int getPrecio() {
        return precio;
    }

    public boolean isEsComida() {
        return esComida;
    }

    public ArrayList<Envase> getEnvasesSugeridos() {
        return envasesSugeridos;
    }

    //SETTER'S
    public void setMaterial(String material) {
        this.material = material;
    }

    public void setOrigenMateriaPrima(String origenMateriaPrima) {
        this.origenMateriaPrima = origenMateriaPrima;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public void setEsComida(boolean esComida) {
        this.esComida = esComida;
    }

    public void setEnvasesSugeridos(ArrayList<Envase> envasesSugeridos) {
        this.envasesSugeridos = envasesSugeridos;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public static void setCantProductos(int cantProductos) {
        Producto.cantProductos = cantProductos;
    }

}
