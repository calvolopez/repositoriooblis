package dominio;

public class PuntoDeVenta implements Comparable {

    //ATRIBUTOS
    private static int cantLocales = 0;     //Variable GLOBAL de la clase, para llevar el codigo y conocer la cant. de locales
    private int codigo;
    private String calle;
    private String esquina;
    private String barrio;
    private int numPuerta;
    private int telefono;

    //METODOS
    public PuntoDeVenta(String calle, String esquina, String barrio, int numPuerta, int telefono) {
        this.setCodigo(this.getCantLocales() + 1);
        this.setCantLocales(cantLocales + 1);
        this.setCalle(calle);
        this.setEsquina(esquina);
        this.setBarrio(barrio);
        this.setNumeroPuerta(numPuerta);
        this.setTelefono(telefono);
    }

    public PuntoDeVenta() {
        this.setCodigo(this.getCantLocales() + 1);
        this.setCantLocales(cantLocales + 1);
        this.setCalle(null);
        this.setEsquina(null);
        this.setBarrio(null);
        this.setNumeroPuerta(0);
        this.setTelefono(0);
    }

    //OVERRIDE's
    @Override
    public int compareTo(Object o) {
        PuntoDeVenta aux = (PuntoDeVenta) o;

        if (this.codigo > aux.codigo) {
            return 1;
        } else if (this.codigo < aux.codigo) {
            return -1;
        } else {
            return 0;
        }
    }

    //GETTERS
    public String getCalle() {
        return calle;
    }

    public String getEsquina() {
        return esquina;
    }

    public String getBarrio() {
        return barrio;
    }

    public int getNumeroPuerta() {
        return numPuerta;
    }

    public int getCodigo() {
        return codigo;
    }

    public int getTelefono() {
        return telefono;
    }

    public static int getCantLocales() {
        return cantLocales;
    }

    //SETTERS
    public void setCalle(String calle) {
        this.calle = calle;
    }

    public void setEsquina(String esquina) {
        this.esquina = esquina;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public void setNumeroPuerta(int numeroPuerta) {
        this.numPuerta = numeroPuerta;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setTelefono(int tel) {
        this.telefono = tel;
    }

    public static void setCantLocales(int cantLocales) {
        PuntoDeVenta.cantLocales = cantLocales;
    }

}
