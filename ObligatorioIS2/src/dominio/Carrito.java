package dominio;

import java.util.ArrayList;
import java.util.Iterator;
import tads.Asociacion;

public class Carrito {

    //ATRIBUTOS
    private ArrayList<Asociacion<Asociacion<Producto, Integer>, Envase>> productos;
    private ArrayList<Asociacion<Promocion, Integer>> promos;
    private int costo;

    //METODOS
    public Carrito() {
        this.productos = new ArrayList<>();
        this.promos = new ArrayList();
        this.setCosto(0);
    }

    //PRE: -
    //POS: Agrega 'cant' veces el producto p, con el envase sugerido e 
    public void agregarProducto(Producto p, int cant, Envase e) {
        productos.add(new Asociacion(new Asociacion(p, cant), e));
        costo += p.getPrecio() * cant;
    }

    public void eliminarProducto(Producto p, int cant, Envase e) {
        productos.remove(new Asociacion(new Asociacion(p, cant), e));
        costo -= p.getPrecio() * cant;
    }

    //PRE: -
    //POS: Retorna la Cantidad de Kilos que hay en el carrito
    public int cantKilos() {
        int ret = 0;

        Iterator<Asociacion<Asociacion<Producto, Integer>, Envase>> itCarrito = this.getProductos().iterator();
        while (itCarrito.hasNext()) {
            Asociacion<Asociacion<Producto, Integer>, Envase> a = itCarrito.next();
            Producto p = a.getDominio().getDominio();
            int cant = a.getDominio().getRango();

            if (p.isEsComida()) {
                ret += cant;
            }
        }

        Iterator<Asociacion<Promocion, Integer>> itPromos = this.getPromos().iterator();
        while (itPromos.hasNext()) {
            Asociacion<Promocion, Integer> a = itPromos.next();
            Promocion promo = a.getDominio();
            int cant = a.getRango();

            if (promo.getProducto().isEsComida()) {
                if (promo.getEsDescuento()) {
                    ret += cant;
                } else {
                    ret += cant * promo.getValor();
                }
            }
        }
        return ret;
    }

    //PRE: 
    //POS: Retorna la Cantidad de Litros que hay en el carrito
    public int cantLitros() {
        int ret = 0;

        Iterator<Asociacion<Asociacion<Producto, Integer>, Envase>> itCarrito = this.getProductos().iterator();
        while (itCarrito.hasNext()) {
            Asociacion<Asociacion<Producto, Integer>, Envase> a = itCarrito.next();
            Producto p = a.getDominio().getDominio();
            int cant = a.getDominio().getRango();

            if (!p.isEsComida()) {
                ret += cant;
            }
        }

        Iterator<Asociacion<Promocion, Integer>> itPromos = this.getPromos().iterator();
        while (itPromos.hasNext()) {
            Asociacion<Promocion, Integer> a = itPromos.next();
            Promocion promo = a.getDominio();
            int cant = a.getRango();

            if (!promo.getProducto().isEsComida()) {
                if (promo.getEsDescuento()) {
                    ret += cant;
                } else {
                    ret += cant * promo.getValor();
                }
            }
        }
        return ret;
    }

    //GETTER'S
    public int getCosto() {
        return costo;
    }

    public ArrayList<Asociacion<Asociacion<Producto, Integer>, Envase>> getProductos() {
        return productos;
    }

    public ArrayList<Asociacion<Promocion, Integer>> getPromos() {
        return promos;
    }

    //SETTER'S
    public void setCosto(int costo) {
        this.costo = costo;
    }

    public void setProductos(ArrayList<Asociacion<Asociacion<Producto, Integer>, Envase>> productos) {
        this.productos = productos;
    }

    public void setPromos(ArrayList<Asociacion<Promocion, Integer>> promos) {
        this.promos = promos;
    }

}
