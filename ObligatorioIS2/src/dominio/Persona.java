package dominio;

public abstract class Persona {

    //ATRIBUTOS
    private String nombre;
    private String email;

    //METODOS
    public Persona() {
        this.setEmail(null);
        this.setNombre(null);
    }

    //SETTER
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    //GETTER
    public String getNombre() {
        return this.nombre;
    }

    public String getEmail() {
        return this.email;
    }
}
