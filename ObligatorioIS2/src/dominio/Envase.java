package dominio;

public class Envase implements Comparable {

    //ATRIBUTOS
    private static int cantEnvases = 0;                         //Variable GLOBAL de la clase para llevar el codigo del envase
    private static int[] tablaCantidadUsados = new int[200];    //Array donde tabla[i] = cantReusados (para el envase con codigo 'i')

    private String descripcion;
    private String nombre;
    private int codigo;                                         //Codigo UNICO
    private String urlImg;

    public Envase(String descripcion, String nombre, String url) {
        this.setDescripcion(descripcion);
        this.setNombre(nombre);
        this.setCodigo(cantEnvases + 1);
        this.setCantEnvases(cantEnvases + 1);
        this.setUrlImg(url);
        tablaCantidadUsados[this.getCodigo()] = 0;
    }

    public Envase() {
        this.setDescripcion("");
        this.setNombre("");
        this.setCodigo(cantEnvases + 1);
        this.setCantEnvases(cantEnvases + 1);
        this.setUrlImg("");
        tablaCantidadUsados[this.getCodigo()] = 0;
    }

    //PRE: -
    //POS: Suma 'cantUsosNueva' a la tabla para el Envase
    public void agregarCantidadUsos(int cantUsosNueva) {
        int codigoEnvase = this.getCodigo();
        if (codigoEnvase <= cantEnvases) {
            tablaCantidadUsados[codigoEnvase] += cantUsosNueva;
        }
    }

    //PRE: -
    //POS: Retorna la cantidad de veces que se uso el Envase
    public int cantidadDeUsos() {
        return tablaCantidadUsados[this.getCodigo()];
    }

    //PRE: -
    //POS: Elimina el envase. Ajusta el array static, ajusta cantElem
    public void eliminarEnvase(int codigo) {
        this.setCantEnvases(cantEnvases - 1);;

        for (int i = codigo; i < tablaCantidadUsados.length - 1; i++) {
            tablaCantidadUsados[i] = tablaCantidadUsados[i + 1];
        }
    }

    //PRE: -
    //POS: Retorna el envase mas reutilizado
    public int envaseMasReutilizado() {
        int max = 0;

        //Buscamos el maximo
        for (int i = 0; i < tablaCantidadUsados.length; i++) {
            if (tablaCantidadUsados[i] > max) {
                max = tablaCantidadUsados[i];
            }
        }

        //Retornamos el codigo del maximo
        for (int i = 0; i < tablaCantidadUsados.length; i++) {
            if (tablaCantidadUsados[i] == max) {
                return i;
            }
        }

        return 0;
    }

    @Override   //Comparamos por codigo
    public int compareTo(Object o) {
        Envase aux = (Envase) o;
        if (this.codigo > aux.codigo) {
            return 1;
        } else if (this.codigo < aux.codigo) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return this.nombre;
    }

    //GETTERS
    public int getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public static int getCantEnvases() {
        return cantEnvases;
    }

    public String getNombre() {
        return nombre;
    }

    public String getUrlImg() {
        return urlImg;
    }

    //SETTERS
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setUrlImg(String url) {
        this.urlImg = url;
    }

    public static void setCantEnvases(int cantEnvases) {
        Envase.cantEnvases = cantEnvases;
    }

}
