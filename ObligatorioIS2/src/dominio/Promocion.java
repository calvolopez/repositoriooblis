package dominio;

public class Promocion implements Comparable {

    //ATRIBUTOS
    private Producto producto;
    private String descripcion;
    private boolean esDescuento;
    private int valor;
    private int valor2;
    private int precio;

    //METODOS
    public Promocion(Producto p, String descr, boolean esDescuento, int valor, int valor2) {
        this.setDescripcion(descr);
        this.setProducto(p);
        this.setEsDescuento(esDescuento);
        this.setValor(valor);
        this.setValor2(valor2);

        if (esDescuento) {
            precio = this.getProducto().getPrecio() * (100 - valor) / 100;
        } else {
            precio = this.getProducto().getPrecio() * valor2;
        }
    }

    public Promocion() {
        this.setDescripcion("");
        this.setProducto(null);
        this.setEsDescuento(true);
        this.setValor(0);
        this.setValor2(0);
    }

    @Override
    public int compareTo(Object o) {
        Promocion aux = (Promocion) o;
        return this.producto.compareTo(aux.getProducto());
    }

    @Override
    public boolean equals(Object o) {
        Promocion aux = (Promocion) o;
        return this.producto.compareTo(aux.getProducto()) == 0;
    }

    //GETTER'S
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean getEsDescuento() {
        return esDescuento;
    }

    public int getValor() {
        return valor;
    }

    public int getValor2() {
        return valor2;
    }

    public int getPrecio() {
        return precio;
    }

    //SETTER'S
    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public void setEsDescuento(boolean esDescuento) {
        this.esDescuento = esDescuento;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public void setValor2(int valor2) {
        this.valor2 = valor2;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }
}
