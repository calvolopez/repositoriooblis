package dominio;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import tads.Asociacion;

public class Venta implements Comparable {

    //ATRIBUTOS
    private static int cantVentas = 0;      //Variable GLOBAL de la clase: Para conocer el codigo del producto y la cant de ventas
    private ArrayList<Asociacion<Asociacion<Producto, Integer>, Envase>> productos;     //Para cada PRODUCTO, su CANTIDAD
    private ArrayList<Asociacion<Promocion, Integer>> promos;
    private ArrayList<Envase> envasesUsados;
    private int costo;
    private int codigo;
    private String rutEmpresa = "1234";
    private boolean preventa;
    private ClienteOnline cliente;
    private Date fechaVenta;
    private Date fechaRetiro;

    //METODOS    
    public Venta(ArrayList<Asociacion<Asociacion<Producto, Integer>, Envase>> productos, ArrayList<Asociacion<Promocion, Integer>> promos, int costo, ClienteOnline cliente, Date fechaVenta, Date fechaPreventa, boolean preventa) {
        this.setProductos(productos);

        //Para cada envase usado, si no pertenece a la lista, lo agregamos
        ArrayList<Envase> listaEnvases = new ArrayList();
        for (int i = 0; i < productos.size(); i++) {
            Envase e = productos.get(i).getRango();
            if (!listaEnvases.contains(e)) {
                listaEnvases.add(e);
            }
        }

        this.setEnvasesUsados(listaEnvases);
        this.setCosto(costo);
        this.setCodigo(this.getCantVentas() + 1);
        this.setCantVentas(cantVentas + 1);
        this.setCliente(cliente);
        this.setFechaVenta(fechaVenta);
        this.setFechaRetiro(fechaPreventa);
        this.setPreventa(preventa);
        this.setPromos(promos);
    }

    public Venta() {
        this.setProductos(new ArrayList<>());
        this.setEnvasesUsados(new ArrayList<>());
        this.setCosto(0);
        this.setCodigo(this.getCantVentas() + 1);
        this.setCantVentas(cantVentas + 1);
        this.setCliente(new ClienteOnline());
        Calendar c = Calendar.getInstance();
        Date d = c.getTime();
        this.setFechaVenta(d);
        this.setFechaRetiro(d);
        this.setPreventa(false);
    }

    @Override
    public int compareTo(Object o) {
        Venta aux = (Venta) o;

        if (this.codigo > aux.codigo) {
            return 1;
        } else if (this.codigo < aux.codigo) {
            return -1;
        } else {
            return 0;
        }
    }

    //GETTERS
    public ArrayList<Asociacion<Asociacion<Producto, Integer>, Envase>> getProductos() {
        return productos;
    }

    public static int getCantVentas() {
        return cantVentas;
    }

    public ArrayList<Envase> getEnvasesUsados() {
        return envasesUsados;
    }

    public int getCosto() {
        return costo;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getRutEmpresa() {
        return rutEmpresa;
    }

    public boolean isPreventa() {
        return preventa;
    }

    public ClienteOnline getCliente() {
        return cliente;
    }

    public Date getFechaVenta() {
        return fechaVenta;
    }

    public Date getFechaRetiro() {
        return fechaRetiro;
    }

    public ArrayList<Asociacion<Promocion, Integer>> getPromos() {
        return promos;
    }

    //SETTERS
    public void setProductos(ArrayList<Asociacion<Asociacion<Producto, Integer>, Envase>> productos) {
        this.productos = productos;
    }

    public void setEnvasesUsados(ArrayList<Envase> envasesUsados) {
        this.envasesUsados = envasesUsados;
    }

    public void setCosto(int costo) {
        this.costo = costo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setRutEmpresa(String rutEmpresa) {
        this.rutEmpresa = rutEmpresa;
    }

    public void setPreventa(boolean preventa) {
        this.preventa = preventa;
    }

    public void setCliente(ClienteOnline cliente) {
        this.cliente = cliente;
    }

    public void setFechaVenta(Date f) {
        this.fechaVenta = f;
    }

    public void setFechaRetiro(Date f) {
        this.fechaRetiro = f;
    }

    public void setPromos(ArrayList<Asociacion<Promocion, Integer>> promos) {
        this.promos = promos;
    }

    public static void setCantVentas(int cantVentas) {
        Venta.cantVentas = cantVentas;
    }

}
