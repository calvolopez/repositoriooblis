package dominio;

import java.util.*;
import tads.Asociacion;

public class Sistema {

    //ATRIBUTOS
    private ArrayList<Producto> productos;
    private ArrayList<Envase> envases;
    private ArrayList<Venta> ventas;
    private ArrayList<Venta> preVentas;
    private ArrayList<ClienteOnline> clientes;
    private ArrayList<PuntoDeVenta> puntosDeVenta;
    private ArrayList<Venta>[] ventasPorMes = new ArrayList[13];
    private ArrayList<Promocion> promos;
    private Carrito carrito;
    ClienteOnline cliente;

    //METODOS
    public Sistema() {
        ventasPorMes = new ArrayList[13];
        for (int i = 0; i < 13; i++) {
            ventasPorMes[i] = new ArrayList();
        }

        this.clientes = new ArrayList();
        this.productos = new ArrayList();
        this.envases = new ArrayList();
        this.puntosDeVenta = new ArrayList();
        this.ventas = new ArrayList();
        this.preVentas = new ArrayList();
        this.promos = new ArrayList();
        this.carrito = new Carrito();
    }

    //  ESTADISTICA ------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------
    //PRE: -
    //POS: Recibe un Producto ==> Retorna la cantidad vendida
    public int cantVendidos(Producto p) {
        return p.cantidadVendidos();
    }

    //PRE: - 
    //POS: Retorna la cantidad de kg vendidos
    public int kilosVendidos() {
        int retorno = 0;

        Iterator<Producto> itProds = this.productos.iterator();
        Producto p;
        while (itProds.hasNext()) {
            p = itProds.next();
            if (p.isEsComida()) {
                retorno += p.cantidadVendidos();
            }
        }
        return retorno;
    }

    //PRE: - 
    //POS: Retorna la cantidad de kg vendidos
    public int litrosVendidos() {
        int retorno = 0;

        Iterator<Producto> itProds = this.productos.iterator();
        Producto p;
        while (itProds.hasNext()) {
            p = itProds.next();
            if (!p.isEsComida()) {
                retorno += p.cantidadVendidos();
            }
        }
        return retorno;
    }

    //PRE: -
    //POS: Retorna la cantidad de productos vendidos
    public int cantTotalProductosVendidos() {
        Iterator<Producto> itProds = productos.iterator();
        int retorno = 0;

        while (itProds.hasNext()) {
            retorno += cantVendidos(itProds.next());
        }

        return retorno;
    }

    //PRE: -
    //POS: Recibe un Envase ==> Retorna la cantidad de veces usado
    public int cantVecesUsado(Envase e) {
        return e.cantidadDeUsos();
    }

    //PRE: -
    //POS: Retorna la cantidad de envases reutilizados (la suma de canVecesUsados para cada Envase)
    public int cantEnvasesReutilizados() {
        int retorno = 0;
        Iterator<Envase> iteradorEnvases = envases.iterator();

        while (iteradorEnvases.hasNext()) {
            retorno += iteradorEnvases.next().cantidadDeUsos();
        }

        return retorno;
    }

    //PRE: -
    //POS: Retorna la cantidad de Ventas en un mes dado
    public int cantVentasEnMes(int mes) {
        return this.getVentasPorMes()[mes].size();
    }

    //PRE: -
    //POS: Retorna la cantidad de plata recaudada en un mes dado
    public int cantRecaudadaEnMes(int mes) {
        int retorno = 0;
        ArrayList<Venta> ventas = this.getVentasPorMes()[mes];

        for (int i = 0; i < ventas.size(); i++) {
            retorno = retorno + ventas.get(i).getCosto();
        }

        return retorno;
    }

    //PRE: -
    //POS: Retorna el Producto mas vendido
    public Producto productoMasVendido() {
        int codigoDelMasVendido = this.getProductos().get(0).productoMasVendido();
        Producto retorno = null;
        boolean seguir = true;

        Iterator<Producto> iteradorProductos = this.getProductos().iterator();
        while (iteradorProductos.hasNext() && seguir) {
            retorno = iteradorProductos.next();
            if (retorno.getCodigo() == codigoDelMasVendido) {
                seguir = false;
            }
        }

        return retorno;
    }

    //PRE: -
    //POS: Retorna el Envase mas reutilizado
    public Envase envaseMasReutilizado() {
        int codigoDelMasVendido = this.getEnvases().get(0).envaseMasReutilizado();
        Envase retorno = null;
        boolean seguir = true;

        Iterator<Envase> itEnvases = this.getEnvases().iterator();
        while (itEnvases.hasNext() && seguir) {
            retorno = itEnvases.next();
            if (retorno.getCodigo() == codigoDelMasVendido) {
                seguir = false;
            }
        }

        return retorno;
    }

    //  AGREGAR Elemento -------------------------------------------------------------------
    // -------------------------------------------------------------------------------------
    public void agregarProducto(Producto p) {
        this.getProductos().add(p);
    }

    public void agregarEnvase(Envase e) {
        this.getEnvases().add(e);
    }

    //PRE: -
    //POS: Agrega la venta a la ventas, y tambien a preventa (si lo amerita). \
    //Ademas, ajusta la cantidad de envases utilizados, y de productos vendidos.
    public void agregarVenta(Venta v) {
        if (v.isPreventa()) {
            this.preVentas.add(v);
        }

        this.getVentas().add(v);
        this.getVentasPorMes()[v.getFechaVenta().getMonth()].add(v);

        //Sumamos la cantidad de Usos para cada envase
        ArrayList<Envase> envasesUsados = v.getEnvasesUsados();
        for (int i = 0; i < envasesUsados.size(); i++) {
            envasesUsados.get(i).agregarCantidadUsos(1);
        }

        //Sumamos la cantidad de ventas para cada producto
        ArrayList< Asociacion<Asociacion<Producto, Integer>, Envase>> productosVendidos = v.getProductos();
        for (int i = 0; i < productosVendidos.size(); i++) {
            //Para el producto de la Asociacion, le sumamos el Integer de la asociacion a su cantVentas
            productosVendidos.get(i).getDominio().getDominio().agregarCantidadVendidos(productosVendidos.get(i).getDominio().getRango());
        }
    }

    public void agregarPromo(Promocion p) {
        this.getPromos().add(p);
    }

    public void agregarCliente(ClienteOnline c) {
        this.getClientes().add(c);
    }

    public void agregarPuntoVenta(PuntoDeVenta pto) {
        this.getPuntosDeVenta().add(pto);
    }

    //  ELIMINAR Elemento ------------------------------------------------------------------
    // -------------------------------------------------------------------------------------    
    //PRE: - 
    //POS: Elimina el producto p de productos, y del array. Reajustando los valores
    public void eliminarProducto(Producto p) {
        this.getProductos().get(0).eliminarProducto(p.getCodigo());     //Ajustamos la tabla de cantVendidos y cantProd--
        this.getProductos().remove(p);                                  //Eliminamos 'p' de 'productos'
        Producto aux;

        //Ajustamos el codigo de los prods con codigo > que 'p' , al numero anterior
        Iterator<Producto> itProductos = this.getProductos().iterator();
        while (itProductos.hasNext()) {
            aux = itProductos.next();
            if (aux.getCodigo() > p.getCodigo()) {
                aux.setCodigo(aux.getCodigo() - 1);
            }
        }
    }

    //PRE: -
    //POS: Elimina la promocion de promos, y del carrito
    public void eliminarPromo(Promocion p) {
        this.getPromos().remove(p);

        Asociacion<Promocion, Integer> aux = new Asociacion(p, 0);
        this.getCarrito().getPromos().remove(aux);
    }

    //PRE: -
    //POS: Elimina 'e' de envases y reajusta los valores.
    public void eliminarEnvase(Envase e) {
        Envase aux;
        this.getEnvases().get(0).eliminarEnvase(e.getCodigo());     //Ajustamos la tabla de cantUsados y cantEnvases--
        this.getEnvases().remove(e);                                //Elimanos el 'e' de 'envases'

        //Ajustamos el codigo de los envases con codigo mayor que 'e' a uno menos
        Iterator<Envase> itEnvases = this.getEnvases().iterator();
        while (itEnvases.hasNext()) {
            aux = itEnvases.next();
            if (aux.getCodigo() > e.getCodigo()) {
                aux.setCodigo(aux.getCodigo() - 1);
            }
        }
    }

    //PRE: Todos los productos/envases de 'v' pertenecen a 'productos'/'envases'
    //POS: Elimina la Venta 'v' del sistema
    public void eliminarVenta(Venta v) {
        //Haremos lo contrario a agregarVenta()

        //Ajustamos tablaCantEnvasesReutilizados
        ArrayList<Envase> envasesUsados = v.getEnvasesUsados();
        for (int i = 0; i < envasesUsados.size(); i++) {
            envasesUsados.get(i).agregarCantidadUsos(-1);
        }

        //Ajustamos tablaCantidadVendidos
        ArrayList< Asociacion<Asociacion<Producto, Integer>, Envase>> productosVendidos = v.getProductos();
        for (int i = 0; i < productosVendidos.size(); i++) {
            //Para el producto de la Asociacion, le restamos el Integer de la asociacion a su cantVentas
            productosVendidos.get(i).getDominio().getDominio().agregarCantidadVendidos(-productosVendidos.get(i).getDominio().getRango());
        }

        this.getVentas().remove(v);
        this.getVentasPorMes()[v.getFechaVenta().getMonth()].remove(v);
    }

    public void eliminarCliente(ClienteOnline c) {
        this.getClientes().remove(c);
    }

    public void eliminarPuntoVenta(PuntoDeVenta pto) {
        this.getPuntosDeVenta().remove(pto);
    }

    //PRE: -
    //POS: Vacia el carrito de compras
    public void vaciarCarrito() {
        this.getCarrito().setCosto(0);
        this.getCarrito().setProductos(new ArrayList());
        this.getCarrito().setPromos(new ArrayList());
    }

    //  AUXILIARES -------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------
    //PRE: -
    //POS: True sii 'dato' es un numero
    public boolean esUnNumero(String dato) {
        boolean ret = true;
        try {
            Integer.parseInt(dato);
        } catch (NumberFormatException e) {
            ret = false;
        }
        return ret;
    }

    //PRE: -
    //POS: True sii 'dato' es una cantidad valida
    public boolean ingresoCantidadValido(String dato) {
        if (!esUnNumero(dato)) {
            return false;
        }
        int num = Integer.parseInt(dato);
        if (num < 0) {
            return false;
        }
        if (num > 10) {
            return false;
        }
        return true;
    }

    // GETTERS & SETTERS -------------------------------------------------------------------
    // GETTERS -----------------------------------------------------------------------------
    public ArrayList<Producto> getProductos() {
        return this.productos;
    }

    public ArrayList<Envase> getEnvases() {
        return this.envases;
    }

    public ArrayList<Venta> getVentas() {
        return this.ventas;
    }

    public ArrayList<ClienteOnline> getClientes() {
        return this.clientes;
    }

    public ArrayList<PuntoDeVenta> getPuntosDeVenta() {
        return this.puntosDeVenta;
    }

    public ArrayList<Venta>[] getVentasPorMes() {
        return this.ventasPorMes;
    }

    public Carrito getCarrito() {
        return carrito;
    }

    public ArrayList<Promocion> getPromos() {
        return promos;
    }

    public ClienteOnline getCliente() {
        return cliente;
    }

    //SETTER
    public void setCarrito(Carrito c) {
        this.carrito = c;
    }

    public void setPromos(ArrayList<Promocion> promos) {
        this.promos = promos;
    }

    public void setCliente(ClienteOnline cliente) {
        this.cliente = cliente;
    }

}
