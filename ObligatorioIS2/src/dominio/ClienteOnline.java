package dominio;

import java.util.ArrayList;

public class ClienteOnline extends Persona implements Comparable {

    //ATRIBUTOS
    private static int cantClientes = 0;    //Variable GLOBAL para llevar el numeroCliente

    private int numeroCliente;
    private int edad;
    private String apellido;
    private String empresa;
    private String contrasena;
    private String domicilio;
    private ArrayList<Venta> compras;

    //METODOS
    public ClienteOnline(String nombre, String apellido, String mail) {
        this.setApellido(apellido);
        this.setNombre(nombre);
        this.setEmail(mail);
        this.setCompras(new ArrayList<Venta>());
        this.setNumeroCliente(cantClientes + 1);
        this.setCantClientes(cantClientes + 1);
    }

    public ClienteOnline() {
        this.setNumeroCliente(cantClientes + 1);
        this.setCantClientes(cantClientes + 1);
        this.edad = 0;
        this.apellido = null;
        this.empresa = null;
        this.contrasena = null;

        this.setCompras(new ArrayList());
        this.setNombre(null);
        this.setEmail("");
    }

    //PRE: -
    //POS: retorna la cantidad de clientes
    public int cantidadClientes() {
        return cantClientes;
    }

    @Override   //Comparamos por numeroCliente
    public int compareTo(Object o) {
        ClienteOnline aux = (ClienteOnline) o;

        if (this.numeroCliente > aux.numeroCliente) {
            return 1;
        } else if (this.numeroCliente < aux.numeroCliente) {
            return -1;
        } else {
            return 0;
        }
    }

    //GETTERS
    public int getNumeroCliente() {
        return numeroCliente;
    }

    public int getEdad() {
        return edad;
    }

    public String getApellido() {
        return apellido;
    }

    public String getEmpresa() {
        return empresa;
    }

    public String getContrasena() {
        return contrasena;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public ArrayList<Venta> getCompras() {
        return compras;
    }

    //SETTERS
    public void setNumeroCliente(int numeroCliente) {
        this.numeroCliente = numeroCliente;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public void setCompras(ArrayList<Venta> compras) {
        this.compras = compras;
    }

    public void agregarVenta(Venta v) {
        this.getCompras().add(v);
    }

    public static void setCantClientes(int cantClientes) {
        ClienteOnline.cantClientes = cantClientes;
    }

}
