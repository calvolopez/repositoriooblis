package tads;

/*      //El 2DO Mayor
        Object o = new Asociacion<D, R>(dMayor, rango);
        Asociacion instance = new Asociacion<D, R>(dMenor, rango);
        
        int expResult = 0;
        int result = instance.compareTo(o);
        System.out.println( (result == -1) ? "   Funciona CompareTo" : "   Revisar CompareTo");*/

public class Asociacion<D extends Comparable, R> implements Comparable{
    //  ATRIBUTOS
    private D dominio;
    private R rango;


    //  METODOS ---------------------------------------
    public Asociacion(D dom, R rango)
    {
        this.dominio = dom; //En COLA PRIORIDAD: El valor de la clave
        this.rango = rango;  //En COLA PRIORIDAD: Sera la Prioridad
    }
    
    public Asociacion()
    {
        this.dominio = null;
        this.rango = null;
    }

    @Override
    public int compareTo(Object o)
    {
        Asociacion<D,R> a = (Asociacion<D,R>) o;
        if(a != null && this != null)
        {
            return this.getDominio().compareTo(a.getDominio());
        }
        else if (a == null)
        {
            return 1;
        }
        else return -1;   
    }

    @Override
    public boolean equals(Object o)
    {
        Asociacion<D,R> a = (Asociacion<D,R>) o;
        if(this.compareTo(a) == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    //  GETTERS &&  SETTERS ------------------------------
    public D getDominio() {
        return dominio;
    }

    public R getRango() {
        return rango;
    }

    public void setDominio(D dominio) {
        this.dominio = dominio;
    }

    public void setRango(R rango) {
        this.rango = rango;
    }
}
