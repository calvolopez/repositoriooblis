/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NCatala
 */
public class PromocionTest {
    
    public PromocionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of getDescripcion method, of class Promocion.
     */
    @Test
    public void testGetDescripcion() {
        System.out.println("getDescripcion");
        Promocion instance = new Promocion();
        instance.setDescripcion("Barata");
        String expResult = "Barata";
        String result = instance.getDescripcion();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

   

    /**
     * Test of getEsDescuento method, of class Promocion.
     */
    @Test
    public void testGetEsDescuento() {
        System.out.println("getEsDescuento");
        Promocion instance = new Promocion();
        boolean expResult = true;
        boolean result = instance.getEsDescuento();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getValor method, of class Promocion.
     */
    @Test
    public void testGetValor() {
        System.out.println("getValor");
        Promocion instance = new Promocion();
        int expResult = 0;
        int result = instance.getValor();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of getValor2 method, of class Promocion.
     */
    @Test
    public void testGetValor2() {
        System.out.println("getValor2");
        Promocion instance = new Promocion();
        int expResult = 0;
        int result = instance.getValor2();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

   

    /**
     * Test of getPrecio method, of class Promocion.
     */
    @Test
    public void testGetPrecio() {
        System.out.println("getPrecio");
        Promocion instance = new Promocion();
        instance.setPrecio(23);
        int expResult = 23;
        int result = instance.getPrecio();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getProducto method, of class Promocion.
     */
    @Test
    public void testGetProducto() {
        System.out.println("getProducto");
        Promocion instance = new Promocion();
        Producto p = new Producto();
        instance.setProducto(p);
        Producto expResult = p;
        Producto result = instance.getProducto();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

   

    /**
     * Test of setEsDescuento method, of class Promocion.
     */
    @Test
    public void testSetEsDescuento() {
        System.out.println("setEsDescuento");
        boolean esDescuento = false;
        Promocion instance = new Promocion();
        instance.setEsDescuento(esDescuento);
        assertEquals(instance.getEsDescuento(),esDescuento);

// TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setValor method, of class Promocion.
     */
    @Test
    public void testSetValor() {
        System.out.println("setValor");
        int valor = 3;
        Promocion instance = new Promocion();
        instance.setValor(valor);
        int result = instance.getValor();
        assertEquals(result, valor);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setValor2 method, of class Promocion.
     */
    @Test
    public void testSetValor2() {
        System.out.println("setValor2");
        int valor2 = 0;
        Promocion instance = new Promocion();
        instance.setValor2(valor2);
        int result = instance.getValor2();
        assertEquals(result,valor2);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setPrecio method, of class Promocion.
     */
    @Test
    public void testSetPrecio() {
        System.out.println("setPrecio");
        int precio = 10;
        Promocion instance = new Promocion();
        instance.setPrecio(precio);
        int result= instance.getPrecio();
        assertEquals(result,precio);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
