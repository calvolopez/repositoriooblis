package dominio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ClienteOnlineTest {
    
    public ClienteOnlineTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compareTo method, of class ClienteOnline.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        ClienteOnline c2 = new ClienteOnline();
        ClienteOnline instance = new ClienteOnline();
        
        int expResult = 1;
        int result = instance.compareTo(c2);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumeroCliente method, of class ClienteOnline.
     */
    @Test
    public void testGetNumeroCliente() {
        System.out.println("getNumeroCliente");
        ClienteOnline instance = new ClienteOnline();

        int expResult = instance.cantidadClientes();
        int result = instance.getNumeroCliente();

        assertEquals(expResult, result);
    }
    
    /**
     * Test of cantidadClientes method, of class ClienteOnline.
     */
    @Test
    public void testCantidadClientes() {
        System.out.println("cantidadClientes");
        ClienteOnline instance = new ClienteOnline();

        
        int expResult = 12;
        int result = instance.cantidadClientes();

        assertEquals(expResult, result);
    }

    /**
     * Test of getEdad method, of class ClienteOnline.
     */
    @Test
    public void testGetEdad() {
        System.out.println("getEdad");
        ClienteOnline instance = new ClienteOnline();
        instance.setEdad(7);
        
        int expResult = 7;
        int result = instance.getEdad();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getApellido method, of class ClienteOnline.
     */
    @Test
    public void testGetApellido() {
        System.out.println("getApellido");
        ClienteOnline instance = new ClienteOnline();
        instance.setApellido("apellido");
        
        String expResult = "apellido";
        String result = instance.getApellido();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getEmpresa method, of class ClienteOnline.
     */
    @Test
    public void testGetEmpresa() {
        System.out.println("getEmpresa");
        ClienteOnline instance = new ClienteOnline();
        instance.setEmpresa("empresa");
        
        String expResult = "empresa";
        String result = instance.getEmpresa();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getContrasena method, of class ClienteOnline.
     */
    @Test
    public void testGetContrasena() {
        System.out.println("getContrasena");
        ClienteOnline instance = new ClienteOnline();
        instance.setContrasena("contra");
        
        String expResult = "contra";
        String result = instance.getContrasena();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of setNumeroCliente method, of class ClienteOnline.
     */
    @Test
    public void testSetNumeroCliente() {
        System.out.println("setNumeroCliente");
        int numeroCliente = 9;
        ClienteOnline instance = new ClienteOnline();
        instance.setNumeroCliente(numeroCliente);
        
        assertEquals(numeroCliente, instance.getNumeroCliente());
    }

    /**
     * Test of setEdad method, of class ClienteOnline.
     */
    @Test
    public void testSetEdad() {
        System.out.println("setEdad");        
        int edad = 5;
        ClienteOnline instance = new ClienteOnline();
        instance.setEdad(edad);
        
        assertEquals(edad, instance.getEdad());
    }

    /**
     * Test of setApellido method, of class ClienteOnline.
     */
    @Test
    public void testSetApellido() {
        System.out.println("setApellido");
        String apellido = "surname";
        ClienteOnline instance = new ClienteOnline();
        instance.setApellido(apellido);
        
        assertEquals(apellido, instance.getApellido());
    }

    /**
     * Test of setEmpresa method, of class ClienteOnline.
     */
    @Test
    public void testSetEmpresa() {
        System.out.println("setEmpresa");
        String empresa = "company";
        ClienteOnline instance = new ClienteOnline();
        instance.setEmpresa(empresa);
        
        assertEquals(empresa, instance.getEmpresa());
    }

    /**
     * Test of setContrasena method, of class ClienteOnline.
     */
    @Test
    public void testSetContrasena() {
        System.out.println("setContrasena");
        String contrasena = "pass";
        ClienteOnline instance = new ClienteOnline();
        instance.setContrasena(contrasena);
        
        assertEquals(contrasena, instance.getContrasena());
    }
    
}
