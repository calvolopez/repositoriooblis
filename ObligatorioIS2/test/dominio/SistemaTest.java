package dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class SistemaTest {

    public SistemaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of cantVendidos method, of class Sistema.
     */
    @Test
    public void testCantVendidos() {
        System.out.println("cantVendidos");
        Producto p = new Producto();
        p.agregarCantidadVendidos(2);
        Sistema instance = new Sistema();
        instance.agregarProducto(p);
        int expResult = 2;
        int result = instance.cantVendidos(p);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of cantTotalProductosVendidos method, of class Sistema.
     */
    @Test
    public void testCantTotalProductosVendidos() {
        System.out.println("cantTotalProductosVendidos");
        Sistema instance = new Sistema();
        Producto p = new Producto();
        p.agregarCantidadVendidos(2);
        instance.agregarProducto(p);
        Producto p1 = new Producto();
        p.agregarCantidadVendidos(3);
        instance.agregarProducto(p1);
        int expResult = 5;
        int result = instance.cantTotalProductosVendidos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of kilosVendidos method, of class Sistema.
     */
    @Test
    public void testKilosVendidos()
    {
        Sistema instance = new Sistema();
    
        Producto prod = new Producto();
        prod.setEsComida(true);
        prod.agregarCantidadVendidos(7);
        instance.agregarProducto(prod);
        
        int expResult = 7;
        int result = instance.kilosVendidos();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of litrosVendidos method, of class Sistema.
     */
    @Test
    public void testLitrosVendidos()
    {
        Sistema instance = new Sistema();
    
        Producto prod = new Producto();
        prod.setEsComida(false);
        prod.agregarCantidadVendidos(7);
        instance.agregarProducto(prod);
        
        int expResult = 7;
        int result = instance.litrosVendidos();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of cantVecesUsado method, of class Sistema.
     */
    @Test
    public void testCantVecesUsado() {
        System.out.println("cantVecesUsado");
        Envase e = new Envase();
        Sistema instance = new Sistema();
        e.agregarCantidadUsos(4);
        instance.agregarEnvase(e);
        int expResult = 4;
        int result = instance.cantVecesUsado(e);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of cantEnvasesReutilizados method, of class Sistema.
     */
    @Test
    public void testCantEnvasesReutilizados() {
        System.out.println("cantEnvasesReutilizados");
        Sistema instance = new Sistema();
        Envase e = new Envase();
        e.agregarCantidadUsos(4);
        instance.agregarEnvase(e);
        Envase e2 = new Envase();
        e.agregarCantidadUsos(5);
        instance.agregarEnvase(e2);
        int expResult = 9;
        int result = instance.cantEnvasesReutilizados();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of cantVentasEnMes method, of class Sistema.
     */
    @Test
    public void testCantVentasEnMes() {
        System.out.println("cantVentasEnMes");
        int mes = 12;
        Sistema instance = new Sistema();
        int expResult = 0;
        int result = instance.cantVentasEnMes(mes);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of cantRecaudadaEnMes method, of class Sistema.
     */
    @Test
    public void testCantRecaudadaEnMes() {
        System.out.println("cantRecaudadaEnMes");
        int mes = 10;
        Sistema instance = new Sistema();
        Venta v = new Venta();
        v.setCosto(3);
        instance.agregarVenta(v);
        int expResult = 3;
        int result = instance.cantRecaudadaEnMes(mes);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of productoMasVendido method, of class Sistema.
     */
    @Test
    public void testProductoMasVendido() {
        System.out.println("productoMasVendido");
        Sistema instance = new Sistema();
        Producto p = new Producto();
        p.agregarCantidadVendidos(13);
        instance.agregarProducto(p);
        Producto expResult = p;
        Producto result = instance.productoMasVendido();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of envaseMasReutilizado method, of class Sistema.
     */
    @Test
    public void testEnvaseMasReutilizado() {
        System.out.println("envaseMasReutilizado");
        Sistema instance = new Sistema();
        Envase e = new Envase();
        e.agregarCantidadUsos(23);
        Envase expResult = e;
        instance.agregarEnvase(e);
        Envase result = instance.envaseMasReutilizado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of agregarProducto method, of class Sistema.
     */
    @Test
    public void testAgregarProducto() {
        System.out.println("agregarProducto");
        Producto p = new Producto();
        Sistema instance = new Sistema();
        instance.agregarProducto(p);
        assert (instance.getProductos().contains(p));
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of agregarEnvase method, of class Sistema.
     */
    @Test
    public void testAgregarEnvase() {
        System.out.println("agregarEnvase");
        Envase e = new Envase();
        Sistema instance = new Sistema();
        instance.agregarEnvase(e);
        assert (instance.getEnvases().contains(e));
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of agregarVenta method, of class Sistema.
     */
    @Test
    public void testAgregarVenta() {
        System.out.println("agregarVenta");
        Venta v = new Venta();
        Sistema instance = new Sistema();
        instance.agregarVenta(v);
        assert (instance.getVentas().contains(v));
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of agregarPromo method, of class Sistema.
     */
    @Test
    public void testAgregarPromo() {
        System.out.println("agregarPromo");
        Promocion p = new Promocion();
        Sistema instance = new Sistema();
        instance.agregarPromo(p);
        assert (instance.getPromos().size()>0);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of agregarCliente method, of class Sistema.
     */
    @Test
    public void testAgregarCliente() {
        System.out.println("agregarCliente");
        ClienteOnline c = new ClienteOnline();
        Sistema instance = new Sistema();
        instance.agregarCliente(c);
        assert (instance.getClientes().contains(c));
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of agregarPuntoVenta method, of class Sistema.
     */
    @Test
    public void testAgregarPuntoVenta() {
        System.out.println("agregarPuntoVenta");
        PuntoDeVenta pto = new PuntoDeVenta();
        Sistema instance = new Sistema();
        instance.agregarPuntoVenta(pto);
        assert (instance.getPuntosDeVenta().contains(pto));
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of eliminarProducto method, of class Sistema.
     */
    @Test
    public void testEliminarProducto() {
        System.out.println("eliminarProducto");
        Producto p = new Producto();
        Sistema instance = new Sistema();
        instance.agregarProducto(p);
        instance.eliminarProducto(p);
        assert (!instance.getProductos().contains(p));
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of eliminarPromo method, of class Sistema.
     */
    @Test
    public void testEliminarPromo() {
        System.out.println("eliminarPromo");
        Producto prod = new Producto();
        Promocion p = new Promocion();
        p.setProducto(prod);
        Sistema instance = new Sistema();
        instance.agregarPromo(p);
        instance.eliminarPromo(p);
        assert (instance.getPromos().size()==0);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of eliminarEnvase method, of class Sistema.
     */
    @Test
    public void testEliminarEnvase() {
        System.out.println("eliminarEnvase");
        Envase e = new Envase();
        Sistema instance = new Sistema();
        instance.agregarEnvase(e);
        instance.eliminarEnvase(e);
        assert (!instance.getEnvases().contains(e));
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of eliminarVenta method, of class Sistema.
     */
    @Test
    public void testEliminarVenta() {
        System.out.println("eliminarVenta");
        Venta v = new Venta();
        Sistema instance = new Sistema();
        instance.agregarVenta(v);
        instance.eliminarVenta(v);
        assert (!instance.getVentas().contains(v));
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of eliminarCliente method, of class Sistema.
     */
    @Test
    public void testEliminarCliente() {
        System.out.println("eliminarCliente");
        ClienteOnline c = new ClienteOnline();
        Sistema instance = new Sistema();
        instance.agregarCliente(c);
        instance.eliminarCliente(c);
        assert (!instance.getClientes().contains(c));
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of eliminarPuntoVenta method, of class Sistema.
     */
    @Test
    public void testEliminarPuntoVenta() {
        System.out.println("eliminarPuntoVenta");
        PuntoDeVenta pto = new PuntoDeVenta();
        Sistema instance = new Sistema();
        instance.agregarPuntoVenta(pto);
        instance.eliminarPuntoVenta(pto);
        assert (!instance.getPuntosDeVenta().contains(pto));
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of vaciarCarrito method, of class Sistema.
     */
    @Test
    public void testVaciarCarrito() {
        System.out.println("vaciarCarrito");
        Sistema instance = new Sistema();
        instance.vaciarCarrito();
        assert (instance.getCarrito().getProductos().size() == 0);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of esUnNumero method, of class Sistema.
     */
    @Test
    public void testEsUnNumero() {
        System.out.println("esUnNumero");
        String dato = "4";
        Sistema instance = new Sistema();
        boolean expResult = true;
        boolean result = instance.esUnNumero(dato);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of esUnNumero method, of class Sistema.
     */
    @Test
    public void testEsUnNumero2() {
        System.out.println("esUnNumero");
        String dato = "hola";
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.esUnNumero(dato);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of ingresoCantidadValido method, of class Sistema.
     */
    @Test
    public void testIngresoCantidadValido() {
        System.out.println("ingresoCantidadValido");
        String dato = "54";
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.ingresoCantidadValido(dato);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of ingresoCantidadValido method, of class Sistema.
     */
    @Test
    public void testIngresoCantidadValido2() {
        System.out.println("ingresoCantidadValido");
        String dato = "hola";
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.ingresoCantidadValido(dato);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    @Test
    public void testIngresoCantidadValido3() {
        System.out.println("ingresoCantidadValido");
        String dato = "-9";
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.ingresoCantidadValido(dato);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    public void testIngresoCantidadValido4() {
        System.out.println("ingresoCantidadValido");
        String dato = "7";
        Sistema instance = new Sistema();
        boolean expResult = true;
        boolean result = instance.ingresoCantidadValido(dato);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getProductos method, of class Sistema.
     */
    @Test
    public void testGetProductos() {
        System.out.println("getProductos");
        Sistema instance = new Sistema();
        Producto p = new Producto();
        instance.agregarProducto(p);
        Producto p1 = new Producto();
        instance.agregarProducto(p1);
        ArrayList<Producto> expResult = new ArrayList();
        expResult.add(p);
        expResult.add(p1);
        ArrayList<Producto> result = instance.getProductos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getEnvases method, of class Sistema.
     */
    @Test
    public void testGetEnvases() {
        System.out.println("getEnvases");
        Sistema instance = new Sistema();
        Envase e = new Envase();
        Envase e2 = new Envase();
        instance.agregarEnvase(e);
        instance.agregarEnvase(e2);
        ArrayList<Envase> expResult = new ArrayList();
        expResult.add(e);
        expResult.add(e2);
        ArrayList<Envase> result = instance.getEnvases();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of getVentas method, of class Sistema.
     */
    @Test
    public void testGetVentas() {
        System.out.println("getVentas");
        Sistema instance = new Sistema();
        Venta v = new Venta();
        Venta v2 = new Venta();
        ArrayList<Venta> expResult = new ArrayList();
        expResult.add(v);
        expResult.add(v2);
        instance.agregarVenta(v);
        instance.agregarVenta(v2);
        ArrayList<Venta> result = instance.getVentas();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of getClientes method, of class Sistema.
     */
    @Test
    public void testGetClientes() {
        System.out.println("getClientes");
        Sistema instance = new Sistema();
        ArrayList<ClienteOnline> expResult = new ArrayList();
        ClienteOnline c = new ClienteOnline();
        expResult.add(c);
        instance.agregarCliente(c);
        ArrayList<ClienteOnline> result = instance.getClientes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of getPuntosDeVenta method, of class Sistema.
     */
    @Test
    public void testGetPuntosDeVenta() {
        System.out.println("getPuntosDeVenta");
        Sistema instance = new Sistema();
        ArrayList<PuntoDeVenta> expResult = new ArrayList();
        PuntoDeVenta p = new PuntoDeVenta();
        expResult.add(p);
        instance.agregarPuntoVenta(p);
        ArrayList<PuntoDeVenta> result = instance.getPuntosDeVenta();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getVentasPorMes method, of class Sistema.
     */
    @Test
    public void testGetVentasPorMes() {
        System.out.println("getVentasPorMes");
        Sistema instance = new Sistema();
        ArrayList[] expResult = new ArrayList[13];
        for (int i = 0; i < 13; i++) {
            expResult[i] = new ArrayList();
        }
        ArrayList[] result = instance.getVentasPorMes();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //  fail("The test case is a prototype.");
    }

    /**
     * Test of getCarrito method, of class Sistema.
     */
    @Test
    public void testGetCarrito() {
        System.out.println("getCarrito");
        Sistema instance = new Sistema();
        Carrito expResult = new Carrito();
        instance.setCarrito(expResult);
        Carrito result = instance.getCarrito();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getPromos method, of class Sistema.
     */
    @Test
    public void testGetPromos() {
        System.out.println("getPromos");
        Sistema instance = new Sistema();
        ArrayList<Promocion> p = new ArrayList();
        instance.setPromos(p);
        ArrayList<Promocion> expResult = p;
        ArrayList<Promocion> result = instance.getPromos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getCliente method, of class Sistema.
     */
    @Test
    public void testGetCliente() {
        System.out.println("getCliente");
        Sistema instance = new Sistema();
        ClienteOnline c = new ClienteOnline();
        ClienteOnline expResult = c;
        instance.setCliente(c);
        ClienteOnline result = instance.getCliente();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of setCarrito method, of class Sistema.
     */
    @Test
    public void testSetCarrito() {
        System.out.println("setCarrito");
        Carrito c = new Carrito();
        Sistema instance = new Sistema();
        instance.setCarrito(c);
        assertEquals(instance.getCarrito(), c);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of setPromos method, of class Sistema.
     */
    @Test
    public void testSetPromos() {
        System.out.println("setPromos");
        ArrayList<Promocion> promos = new ArrayList();
        Sistema instance = new Sistema();
        instance.setPromos(promos);
        assertEquals(instance.getPromos(), promos);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setCliente method, of class Sistema.
     */
    @Test
    public void testSetCliente() {
        System.out.println("setCliente");
        ClienteOnline cliente = new ClienteOnline();
        Sistema instance = new Sistema();
        instance.setCliente(cliente);
        assertEquals(instance.getCliente(), cliente);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

}
