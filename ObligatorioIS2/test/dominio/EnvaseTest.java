/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NCatala
 */
public class EnvaseTest {
    
    public EnvaseTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of agregarCantidadUsos method, of class Envase.
     */
    @Test
    public void testAgregarCantidadUsos() {
        System.out.println("agregarCantidadUsos");
        int cantUsosNueva = 3;
        Envase instance = new Envase();
        instance.agregarCantidadUsos(cantUsosNueva);
        assert(instance.cantidadDeUsos()==cantUsosNueva);
        // TODO review the generated test code and remove the default call to fail.
        
       // fail("The test case is a prototype.");
    }

   
    /**
     * Test of eliminarEnvase method, of class Envase.
     */
    @Test
    public void testEliminarEnvase() {
        System.out.println("eliminarEnvase");
        int codigo = 0;
        Envase instance = new Envase();
        int cantEnvases= instance.getCantEnvases();
        instance.eliminarEnvase(codigo);
        assert(cantEnvases>instance.getCantEnvases());
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of envaseMasReutilizado method, of class Envase.
     */
    @Test
    public void testEnvaseMasReutilizado() {
        System.out.println("envaseMasReutilizado");
        Envase instance = new Envase();
        instance.setCodigo(3);
        instance.agregarCantidadUsos(80);
        int expResult = 3;
        int result = instance.envaseMasReutilizado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of getCodigo method, of class Envase.
     */
    @Test
    public void testGetCodigo() {
        System.out.println("getCodigo");
        Envase instance = new Envase();
        instance.setCodigo(9);
        int expResult = 9;
        int result = instance.getCodigo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDescripcion method, of class Envase.
     */
    @Test
    public void testGetDescripcion() {
        System.out.println("getDescripcion");
        Envase instance = new Envase();
        instance.setDescripcion("Botella");
        String expResult = "Botella";
        String result = instance.getDescripcion();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      //  fail("The test case is a prototype.");
    }

    /**
     * Test of getCantEnvases method, of class Envase.
     */
    @Test
    public void testGetCantEnvases() {
        System.out.println("getCantEnvases");
        int expResult = 6;
        int result = Envase.getCantEnvases();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
     //   fail("The test case is a prototype.");
    }

    /**
     * Test of getNombre method, of class Envase.
     */
    @Test
    public void testGetNombre() {
        System.out.println("getNombre");
        Envase instance = new Envase();
        instance.setNombre("Tupper");
        String expResult = "Tupper";
        String result = instance.getNombre();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getUrlImg method, of class Envase.
     */
    @Test
    public void testGetUrlImg() {
        System.out.println("getUrlImg");
        Envase instance = new Envase();
        instance.setUrlImg("/imagen");
        String expResult = "/imagen";
        String result = instance.getUrlImg();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    
}
