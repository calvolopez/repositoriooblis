package dominio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class PuntoDeVentaTest {
    
    public PuntoDeVentaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compareTo method, of class PuntoDeVenta.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Object o = new PuntoDeVenta();
        PuntoDeVenta instance = new PuntoDeVenta();
        
        int expResult = 1;
        int result = instance.compareTo(o);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getCalle method, of class PuntoDeVenta.
     */
    @Test
    public void testGetCalle() {
        System.out.println("getCalle");
        PuntoDeVenta instance = new PuntoDeVenta();
        String word = "street";
        instance.setCalle(word);
        
        String expResult = word;
        String result = instance.getCalle();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getEsquina method, of class PuntoDeVenta.
     */
    @Test
    public void testGetEsquina() {
        System.out.println("getEsquina");
        PuntoDeVenta instance = new PuntoDeVenta();
        String word = "corner";
        instance.setEsquina(word);
        
        String expResult = word;
        String result = instance.getEsquina();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getBarrio method, of class PuntoDeVenta.
     */
    @Test
    public void testGetBarrio() {
        System.out.println("getBarrio");
        PuntoDeVenta instance = new PuntoDeVenta();
        String word = "barrio";
        instance.setBarrio(word);
        
        String expResult = word;
        String result = instance.getBarrio();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumeroPuerta method, of class PuntoDeVenta.
     */
    @Test
    public void testGetNumeroPuerta() {
        System.out.println("getNumeroPuerta");
        PuntoDeVenta instance = new PuntoDeVenta();
        int num = 5;
        instance.setNumeroPuerta(num);
        
        int expResult = num;
        int result = instance.getNumeroPuerta();
        
        assertEquals(expResult, result);
    }
    
     /**
     * Test of getCodigo method, of class PuntoDeVenta.
     */
    @Test
    public void testGetCodigo() {
        System.out.println("getCodigo");
        PuntoDeVenta instance = new PuntoDeVenta();
        int num = 5;
        instance.setCodigo(num);
        
        int expResult = num;
        int result = instance.getCodigo();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getCantLocales method, of class PuntoDeVenta.
     */
    @Test
    public void testGetCantLocales() {
        System.out.println("getCantLocales");
        int expResult = 4;
        int result = PuntoDeVenta.getCantLocales();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCalle method, of class PuntoDeVenta.
     */
    @Test
    public void testSetCalle() {
        System.out.println("setCalle");
        String word = "calle";
        PuntoDeVenta instance = new PuntoDeVenta();
        instance.setCalle(word);
        
        assertEquals(word, instance.getCalle());
    }

    /**
     * Test of setEsquina method, of class PuntoDeVenta.
     */
    @Test
    public void testSetEsquina() {
        System.out.println("setEsquina");
        String word = "corner";
        PuntoDeVenta instance = new PuntoDeVenta();
        instance.setEsquina(word);
        
        assertEquals(word, instance.getEsquina());
    }


    /**
     * Test of setBarrio method, of class PuntoDeVenta.
     */
    @Test
    public void testSetBarrio() {
        System.out.println("setBarrio");
        String word = "barrio";
        PuntoDeVenta instance = new PuntoDeVenta();
        instance.setBarrio(word);
        
        assertEquals(word, instance.getBarrio());
    }

    /**
     * Test of setNumeroPuerta method, of class PuntoDeVenta.
     */
    @Test
    public void testSetNumeroPuerta() {
        System.out.println("setNumeroPuerta");
        int num = 8;
        PuntoDeVenta instance = new PuntoDeVenta();
        instance.setNumeroPuerta(num);
        
        assertEquals(num, instance.getNumeroPuerta());
    }

    /**
     * Test of setCodigo method, of class PuntoDeVenta.
     */
    @Test
    public void testSetCodigo() {
        System.out.println("setCodigo");
        int num = 8;
        PuntoDeVenta instance = new PuntoDeVenta();
        instance.setCodigo(num);
        
        assertEquals(num, instance.getCodigo());
    }
    
}
