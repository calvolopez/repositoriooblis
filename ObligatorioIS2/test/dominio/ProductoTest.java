/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NCatala
 */
public class ProductoTest {
    
    public ProductoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of agregarEnvaseSugerido method, of class Producto.
     */
    @Test
    public void testAgregarEnvaseSugerido() {
        System.out.println("agregarEnvaseSugerido");
        Envase e = new Envase();
        Producto instance = new Producto();
        instance.agregarEnvaseSugerido(e);
        assert(instance.getEnvasesSugeridos().contains(e));
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of eliminarEnvaseSugerido method, of class Producto.
     */
    @Test
    public void testEliminarEnvaseSugerido() {
        System.out.println("eliminarEnvaseSugerido");
        Envase e = new Envase();
        Producto instance = new Producto();
        instance.agregarEnvaseSugerido(e);
        instance.eliminarEnvaseSugerido(e);
         assert(!instance.getEnvasesSugeridos().contains(e));
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of agregarCantidadVendidos method, of class Producto.
     */
    @Test
    public void testAgregarCantidadVendidos() {
        System.out.println("agregarCantidadVendidos");
        int cantidadNueva = 3;
        Producto instance = new Producto();
        instance.agregarCantidadVendidos(cantidadNueva);
        assert(instance.cantidadVendidos()==cantidadNueva);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of cantidadVendidos method, of class Producto.
     */
    @Test
    public void testCantidadVendidos() {
        System.out.println("cantidadVendidos");
        Producto instance = new Producto();
        int expResult = 0;
        int result = instance.cantidadVendidos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of productoMasVendido method, of class Producto.
     */
    @Test
    public void testProductoMasVendido() {
        System.out.println("productoMasVendido");
        Producto instance = new Producto();
        instance.agregarCantidadVendidos(90);
        int expResult = instance.getCodigo();
        int result = instance.productoMasVendido();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of eliminarProducto method, of class Producto.
     */
    @Test
    public void testEliminarProducto() {
        System.out.println("eliminarProducto");
        int codigo = 0;
        Producto instance = new Producto();
        int cant = instance.getCantProductos();
        instance.eliminarProducto(codigo);
        assert(instance.getCantProductos() < cant);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    /**
     * Test of getMaterial method, of class Producto.
     */
    @Test
    public void testGetMaterial() {
        System.out.println("getMaterial");
        Producto instance = new Producto();
        instance.setMaterial("Manzana");
        String expResult = "Manzana";
        String result = instance.getMaterial();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getUrlImagen method, of class Producto.
     */
    @Test
    public void testGetUrlImagen() {
        System.out.println("getUrlImagen");
        Producto instance = new Producto();
        instance.setUrlImagen("/img");
        String expResult = "/img";
        String result = instance.getUrlImagen();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of getOrigenMateriaPrima method, of class Producto.
     */
    @Test
    public void testGetOrigenMateriaPrima() {
        System.out.println("getOrigenMateriaPrima");
        Producto instance = new Producto();
        instance.setOrigenMateriaPrima("Africa");
        String expResult = "Africa";
        String result = instance.getOrigenMateriaPrima();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getNombre method, of class Producto.
     */
    @Test
    public void testGetNombre() {
        System.out.println("getNombre");
        Producto instance = new Producto();
        instance.setNombre("Pera");
        String expResult = "Pera";
        String result = instance.getNombre();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of getCodigo method, of class Producto.
     */
    @Test
    public void testGetCodigo() {
        System.out.println("getCodigo");
        Producto instance = new Producto();
        instance.setCodigo(7);
        int expResult = 7;
        int result = instance.getCodigo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of getCantProductos method, of class Producto.
     */
    @Test
    public void testGetCantProductos() {
        System.out.println("getCantProductos");
        int expResult = 1;
        Producto p = new Producto();
        int result = p.getCantProductos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getPrecio method, of class Producto.
     */
    @Test
    public void testGetPrecio() {
        System.out.println("getPrecio");
        Producto instance = new Producto();
        instance.setPrecio(29);
        int expResult = 29;
        int result = instance.getPrecio();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of isEsComida method, of class Producto.
     */
    @Test
    public void testIsEsComida() {
        System.out.println("isEsComida");
        Producto instance = new Producto();
        boolean expResult = false;
        boolean result = instance.isEsComida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      //  fail("The test case is a prototype.");
    }

    /**
     * Test of getEnvasesSugeridos method, of class Producto.
     */
    @Test
    public void testGetEnvasesSugeridos() {
        System.out.println("getEnvasesSugeridos");
        Producto instance = new Producto();
        Envase e = new Envase();
        ArrayList<Envase> envases = new ArrayList<>();
        envases.add(e);
        instance.agregarEnvaseSugerido(e);
        ArrayList<Envase> expResult = envases;
        ArrayList<Envase> result = instance.getEnvasesSugeridos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
   
}
