package dominio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class PersonaTest {
    
    public PersonaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setNombre method, of class Persona.
     */
    @Test
    public void testSetNombre() {
        System.out.println("setNombre");
        String nombre = "name";
        Persona instance = new ClienteOnline();
        instance.setNombre(nombre);
        
        assertEquals(instance.getNombre(), nombre);
    }

    /**
     * Test of setEmail method, of class Persona.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        String email = "mail";
        Persona instance = new ClienteOnline();
        instance.setEmail(email);

        assertEquals(instance.getEmail(), email);
    }

    /**
     * Test of getNombre method, of class Persona.
     */
    @Test
    public void testGetNombre() {
        System.out.println("getNombre");
        Persona instance = new ClienteOnline();
        String nombre = "name";
        instance.setNombre(nombre);
        
        String expResult = nombre;
        String result = instance.getNombre();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getEmail method, of class Persona.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        Persona instance = new ClienteOnline();
        String email = "mail";
        instance.setEmail(email);
        
        String expResult = email;
        String result = instance.getEmail();
        
        assertEquals(expResult, result);
    }  
}
