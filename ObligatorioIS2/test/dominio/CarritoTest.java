/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tads.Asociacion;

/**
 *
 * @author NCatala
 */
public class CarritoTest {

    public CarritoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of agregarProducto method, of class Carrito.
     */
    @Test
    public void testAgregarProducto() {
        System.out.println("agregarProducto");
        Producto p = new Producto();
        int cant = 2;
        Envase e = new Envase();
        Carrito instance = new Carrito();
        instance.agregarProducto(p, cant, e);
        assert (instance.getProductos().contains(new Asociacion(new Asociacion(p, cant), e)));
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of eliminarProducto method, of class Carrito.
     */
    @Test
    public void testEliminarProducto() {
        System.out.println("eliminarProducto");
        Producto p = new Producto();
        int cant = 2;
        Envase e = new Envase();
        Carrito instance = new Carrito();
        instance.eliminarProducto(p, cant, e);
        assert (!instance.getProductos().contains(new Asociacion(new Asociacion(p, cant), e)));
        // TODO review the generated test code and remove the default call to fail.
        //  fail("The test case is a prototype.");
    }

    /**
     * Test of cantKilos method, of class Carrito.
     */
    @Test
    public void testCantKilos() {
        System.out.println("cantKilos");
        Carrito instance = new Carrito();
        int expResult = 0;
        int result = instance.cantKilos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //  fail("The test case is a prototype.");
    }

    @Test
    public void testCantKilos2() {
        System.out.println("cantKilos");
        Carrito instance = new Carrito();
        Producto p = new Producto();
        p.setEsComida(true);
        int cant = 2;
        Envase e = new Envase();
        instance.agregarProducto(p, cant, e);
        int expResult = 2;
        int result = instance.cantKilos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //  fail("The test case is a prototype.");
    }

    @Test
    public void testCantKilos3() {
        System.out.println("cantKilos");
        Carrito instance = new Carrito();
        Promocion promo = new Promocion();
        Producto p = new Producto();
        p.setEsComida(true);
        int cant = 2;
        promo.setProducto(p);
        ArrayList lista = new ArrayList();
        lista.add(new Asociacion(promo, cant));
        instance.setPromos(lista);
        int expResult = 2;
        int result = instance.cantKilos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //  fail("The test case is a prototype.");
    }

    /**
     * Test of cantLitros method, of class Carrito.
     */
    @Test
    public void testCantLitros() {
        System.out.println("cantLitros");
        Carrito instance = new Carrito();
        int expResult = 0;
        int result = instance.cantLitros();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //  fail("The test case is a prototype.");
    }

    @Test
    public void testCantLitros1() {
        System.out.println("cantLitros");
        Carrito instance = new Carrito();
        Producto p = new Producto();
        p.setEsComida(false);
        int cant = 2;
        Envase e = new Envase();
        instance.agregarProducto(p, cant, e);
        int expResult = 2;
        int result = instance.cantLitros();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //  fail("The test case is a prototype.");
    }

    @Test
    public void testCantLitros2() {
        System.out.println("cantLitros");
        Carrito instance = new Carrito();
        Promocion promo = new Promocion();
        Producto p = new Producto();
        p.setEsComida(false);
        int cant = 2;
        promo.setProducto(p);
        ArrayList lista = new ArrayList();
        lista.add(new Asociacion(promo, cant));
        instance.setPromos(lista);
        int expResult = 2;
        int result = instance.cantLitros();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //  fail("The test case is a prototype.");
    }

    /**
     * Test of getCosto method, of class Carrito.
     */
    @Test
    public void testGetCosto() {
        System.out.println("getCosto");
        Carrito instance = new Carrito();
        instance.setCosto(3);
        int expResult = 3;
        int result = instance.getCosto();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //  fail("The test case is a prototype.");
    }

    /**
     * Test of getProductos method, of class Carrito.
     */
    @Test
    public void testGetProductos() {
        System.out.println("getProductos");
        Carrito instance = new Carrito();
        Producto p = new Producto();
        Envase e = new Envase();
        ArrayList<Asociacion<Asociacion<Producto, Integer>, Envase>> productos = new ArrayList<>();
        productos.add(new Asociacion(new Asociacion(p, 2), e));
        instance.setProductos(productos);
        ArrayList<Asociacion<Asociacion<Producto, Integer>, Envase>> expResult = productos;
        ArrayList<Asociacion<Asociacion<Producto, Integer>, Envase>> result = instance.getProductos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

}
