/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tads.Asociacion;

/**
 *
 * @author NCatala
 */
public class VentaTest {

    public VentaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }


    /**
     * Test of getProductos method, of class Venta.
     */
    @Test
    public void testGetProductos() {
        System.out.println("getProductos");
        Venta instance = new Venta();
        Producto p = new Producto();
        Envase e = new Envase();
        ArrayList<Asociacion<Asociacion<Producto, Integer>, Envase>> productos = new ArrayList<>();
        productos.add(new Asociacion(new Asociacion(p, 2), e));
        instance.setProductos(productos);
        ArrayList<Asociacion<Asociacion<Producto, Integer>, Envase>> expResult = productos;
        ArrayList<Asociacion<Asociacion<Producto, Integer>, Envase>> result = instance.getProductos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of getCantVentas method, of class Venta.
     */
    @Test
    public void testGetCantVentas() {
        System.out.println("getCantVentas");
        Venta v = new Venta();
        int expResult = 6;
        int result = v.getCantVentas();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getEnvasesUsados method, of class Venta.
     */
    @Test
    public void testGetEnvasesUsados() {
        System.out.println("getEnvasesUsados");
        Venta instance = new Venta();
        Envase e = new Envase();
        ArrayList<Envase> envases = new ArrayList<>();
        envases.add(e);
        instance.setEnvasesUsados(envases);
        ArrayList<Envase> expResult = envases;
        ArrayList<Envase> result = instance.getEnvasesUsados();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      //  fail("The test case is a prototype.");
    }

    /**
     * Test of getCosto method, of class Venta.
     */
    @Test
    public void testGetCosto() {
        System.out.println("getCosto");
        Venta instance = new Venta();
        int expResult = 0;
        int result = instance.getCosto();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
     //   fail("The test case is a prototype.");
    }

    /**
     * Test of getCodigo method, of class Venta.
     */
    @Test
    public void testGetCodigo() {
        System.out.println("getCodigo");
        Venta instance = new Venta();
        instance.setCodigo(4);
        int expResult = 4;
        int result = instance.getCodigo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      //  fail("The test case is a prototype.");
    }

    /**
     * Test of getRutEmpresa method, of class Venta.
     */
    @Test
    public void testGetRutEmpresa() {
        System.out.println("getRutEmpresa");
        Venta instance = new Venta();
        String expResult = "1234";
        String result = instance.getRutEmpresa();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      //  fail("The test case is a prototype.");
    }

    /**
     * Test of isPreventa method, of class Venta.
     */
    @Test
    public void testIsPreventa() {
        System.out.println("isPreventa");
        Venta instance = new Venta();
        boolean expResult = false;
        boolean result = instance.isPreventa();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      //  fail("The test case is a prototype.");
    }

    /**
     * Test of getCliente method, of class Venta.
     */
    @Test
    public void testGetCliente() {
        System.out.println("getCliente");
        Venta instance = new Venta();
        ClienteOnline expResult = new ClienteOnline();
        instance.setCliente(expResult);
        ClienteOnline result = instance.getCliente();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getFechaVenta method, of class Venta.
     */
    @Test
    public void testGetFechaVenta() {
        System.out.println("getFechaVenta");
        Venta instance = new Venta();
        Calendar c = Calendar.getInstance();
        Date fechaVenta = c.getTime();
        instance.setFechaVenta(fechaVenta);
        Date expResult = fechaVenta;
        Date result = instance.getFechaVenta();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of getFechaRetiro method, of class Venta.
     */
    @Test
    public void testGetFechaRetiro() {
        System.out.println("getFechaRetiro");
        Venta instance = new Venta();
        Calendar c = Calendar.getInstance();
        Date fechaVenta = c.getTime();
        instance.setFechaVenta(fechaVenta);
        Date expResult = fechaVenta;
        Date result = instance.getFechaVenta();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }



}
