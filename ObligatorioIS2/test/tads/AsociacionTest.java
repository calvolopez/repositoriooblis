package tads;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class AsociacionTest {
    
    public AsociacionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compareTo method, of class Asociacion.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        
        Asociacion<String, Integer> a1 = new Asociacion("aaaa",9);
        Asociacion<String, Integer> instance = new Asociacion("aaaa",7);
        
        int expResult = 0;
        int result = instance.compareTo(a1);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getDominio method, of class Asociacion.
     */
    @Test
    public void testGetDominio() {
        System.out.println("getDominio");
        Asociacion<String, Integer> instance = new Asociacion("test", 5);
        
        Object expResult = "test";
        Object result = instance.getDominio();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getRango method, of class Asociacion.
     */
    @Test
    public void testGetRango() {
        System.out.println("getRango");
        Asociacion<String, Integer> instance = new Asociacion("test", 5);
        
        Object expResult = 5;
        Object result = instance.getRango();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of setDominio method, of class Asociacion.
     */
    @Test
    public void testSetDominio() {
        System.out.println("setDominio");
        Object dominio = "nuevo";
        Asociacion<String, Integer> instance = new Asociacion();
        
        instance.setDominio((String) dominio);

        assertEquals(instance.getDominio(), (String) dominio);
    }

    /**
     * Test of setRango method, of class Asociacion.
     */
    @Test
    public void testSetRango() {
        System.out.println("setRango");
        Object rango = 6;
        Asociacion<String, Integer> instance = new Asociacion();
        
        instance.setRango((Integer) rango);
        
        assertEquals(instance.getRango(), (Integer) rango);
    }
    
}
